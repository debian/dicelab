\contentsline {section}{\numberline {1}Introduction to Dicelab}{4}{section.1}
\contentsline {section}{\numberline {2}Installation and Usage}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Building from Source}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Packages}{5}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Windows}{5}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Running}{5}{subsection.2.4}
\contentsline {section}{\numberline {3}The Dicelab Language}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}Introduction}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Reference}{9}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Scalars, Lists and Probabilities}{9}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Grammar}{10}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Dice Operator}{10}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}Arithmetic Operators}{11}{subsubsection.3.2.4}
\contentsline {subsubsection}{\numberline {3.2.5}Scalar Concatenation Operator}{11}{subsubsection.3.2.5}
\contentsline {subsubsection}{\numberline {3.2.6}Summation and Product Operators}{12}{subsubsection.3.2.6}
\contentsline {subsubsection}{\numberline {3.2.7}List Size Operator}{12}{subsubsection.3.2.7}
\contentsline {subsubsection}{\numberline {3.2.8}List Repetition Operator}{12}{subsubsection.3.2.8}
\contentsline {subsubsection}{\numberline {3.2.9}Range Operator}{13}{subsubsection.3.2.9}
\contentsline {subsubsection}{\numberline {3.2.10}List Concatenation Operator}{13}{subsubsection.3.2.10}
\contentsline {subsubsection}{\numberline {3.2.11}List Ordering Operators}{13}{subsubsection.3.2.11}
\contentsline {subsubsection}{\numberline {3.2.12}Low and High Operators}{14}{subsubsection.3.2.12}
\contentsline {subsubsection}{\numberline {3.2.13}First and Last Operators}{14}{subsubsection.3.2.13}
\contentsline {subsubsection}{\numberline {3.2.14}Filtering Operators}{15}{subsubsection.3.2.14}
\contentsline {subsubsection}{\numberline {3.2.15}Let Operator}{15}{subsubsection.3.2.15}
\contentsline {subsubsection}{\numberline {3.2.16}Foreach Operator}{16}{subsubsection.3.2.16}
\contentsline {subsubsection}{\numberline {3.2.17}While Operator}{16}{subsubsection.3.2.17}
\contentsline {subsubsection}{\numberline {3.2.18}If Operator}{16}{subsubsection.3.2.18}
\contentsline {subsection}{\numberline {3.3}Examples}{17}{subsection.3.3}
\contentsline {section}{\numberline {4}Feedback}{18}{section.4}
