#ifndef ROLL_H
#define ROLL_H

struct roll_value {
	int count;
	int *values;
};

struct roll_value *new_roll_single(int num);
struct roll_value *new_roll_multi(int count);
void free_roll(struct roll_value *v);

#endif /* ROLL_H */
