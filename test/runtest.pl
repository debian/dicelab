#!/usr/bin/perl

use strict;
use warnings;

my $testname = shift;
if (! -e $testname) {
	die "Could not find file \"$testname\"";
}

# load the test file
my $fh;
open $fh, $testname 
	or die "Could not open \"$testname\"";
my $mode = 0;
my $line = 1;
my $expression = "";
my %results = ();
while (<$fh>) {
	if (/\[[Ee]xpression\]/) {
		$mode = 1;
	}
	elsif (/\[[Rr]esult\]/) {
		$mode = 2;
	}
	elsif (/^#.*/) {
		# comment
	}
	elsif (/^[ \t]*$/) {
		# empty line
	}
	elsif ($mode == 0) {
		die "Unexpected input on line $line";
	}
	elsif ($mode == 1) {
		chomp;
		$expression .= $_;
	}
	elsif ($mode == 2) {
		if (/^[\t ]*-?[0-9]+[\t ]+[01]\.[0-9]+[ \t]*$/) {
			my ($val, $prop) = split;
			$results{$val} = $prop;
		}
		else {
			die "Unexpected format on line $line";
		}
	}
	$line++;
}
close $fh;

# now run dicelab in -c mode
my $fail = 0;
my %lresults = %results;
open $fh, "echo \"$expression\" | ../dicelab -c |";
while (<$fh>) {
	if (/^[\t ]*-?[0-9]+[\t ]+[01]\.[0-9]+[ \t]*$/) {
		my ($val, $prop) = split;
		if ($lresults{$val} == $prop) {
			delete $lresults{$val};
		}
		else {
			$fail = 1;
			last;
		}
	}
	else {
		$fail = 1;
		last;
	}
}
# see if there is something left
if ((scalar keys %lresults) != 0) {
	$fail = 1;
}
print "$testname (-c): ";
if ($fail) {
	print "FAILED\n";
}
else {
	print "Ok\n";
}
close $fh;

# now run dicelab in -e mode
$fail = 0;
my %eval;
my $count = 1000000;
# XXX this is completely stupid, do it right. the threshold depends on the
# number of results. the following is still stupid, but a wee bit better
my $df = keys %results;
my $thresh = 5 * $df;
open $fh, "echo \"$expression\" | ../dicelab -n $count -e |";
while (<$fh>) {
	if (/^[\t ]*-?[0-9]+[\t ]+[01]\.[0-9]+[ \t]*$/) {
		my ($val, $prop) = split;
		$eval{$val} = $prop;
	}
	else {
		$fail = 1;
		last;
	}
}
# compare
my $chisquare = 0;
foreach my $val (keys %results) {
	if ($results{$val} > 0) {
		$chisquare += (($eval{$val}||0.0) - $results{$val}) * $count
			* (($eval{$val}||0.0) - $results{$val}) * $count
			/ ($results{$val} * $count)
	}
}
if ($chisquare > $thresh) {
	$fail = 1;
}

print "$testname (-e): ";
if ($fail) {
	print "FAILED\n";
}
else {
	print "Ok\n";
}
close $fh;
