dicelab (0.7-7) unstable; urgency=medium

  * Orphaning the package
  * Raising Standards version to 4.6.2 (no change)

 -- Pierre Gruet <pgt@debian.org>  Wed, 16 Aug 2023 17:09:06 +0200

dicelab (0.7-6) unstable; urgency=medium

  * New maintainer, thanks to Robert Lemmen (Closes: #994636)
  * Converting diff from old source 1.0 format into quilt patches
  * Source format is now 3.0 (quilt)
  * Depending on debhelper-compat 13
  * Raising Standards version to 4.6.0:
    - Rules-Requires-Root: no
    - Using the dh sequencer
  * Refreshing d/copyright, using latest format
  * Setting d/watch version to 4
  * Removing now useless debian/dirs file
  * Providing hardening scheme in debian/rules
  * Revising the short description
  * Adding Vcs-* fields pointing to Salsa
  * Fixing a spelling error in the manpage
  * Adding doc-base support
  * Trim trailing whitespace.
  * Removing the build-dependency on inkscape
  * Adding autopkgtests

 -- Pierre Gruet <pgt@debian.org>  Sat, 18 Dec 2021 11:35:57 +0100

dicelab (0.7-5) unstable; urgency=medium

  * Maintenance Release
    * Bumped standards-version and debhelper
    * Added Homepage link

 -- Robert Lemmen <robertle@semistable.com>  Wed, 14 Mar 2018 19:38:33 +0100

dicelab (0.7-4) unstable; urgency=medium

  * Add build-dep to libfl-dev (closes: #846421)

 -- Robert Lemmen <robertle@semistable.com>  Mon, 05 Dec 2016 21:58:27 +0000

dicelab (0.7-3) unstable; urgency=medium

  * Patch extern declaration in main.c to correct return type (closes: #748038)
  * Changes towards lintian-cleanliness
  * Bump standards-version

 -- Robert Lemmen <robertle@semistable.com>  Mon, 06 Jun 2016 21:10:16 +0100

dicelab (0.7-2) unstable; urgency=low

  * Update packaging
  * Bump standards-version
  * Fix a couple of hyphen-vs-minus cases in manpage
  * Enable hardening flags during build
  * Fix a few typos in the package description (closes: #540595)
  * make manpage less confusing around --calc and --count

 -- Robert Lemmen <robertle@semistable.com>  Fri, 05 Jul 2013 09:08:30 +0100

dicelab (0.7-1) unstable; urgency=low

  * New upstream release

 -- Robert Lemmen <robertle@semistable.com>  Wed, 25 Nov 2009 12:30:05 +0000

dicelab (0.6-1) unstable; urgency=low

  * New upstream release

 -- Robert Lemmen <robertle@semistable.com>  Fri, 22 Feb 2008 15:47:40 +0000

dicelab (0.5-1) unstable; urgency=low

  * Initial release (closes: #440822)

 -- Robert Lemmen <robertle@semistable.com>  Mon, 10 Dec 2007 17:06:15 +0100
