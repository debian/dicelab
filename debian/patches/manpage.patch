Description: corrections in the manpage made by former maintainer
Author: Robert Lemmen <robertle@semistable.com>
Origin: upstream
Forwarded: robertle@semistable.com
Last-Update: 2021-12-17

--- a/dicelab.1
+++ b/dicelab.1
@@ -14,8 +14,8 @@
 .BR \-\-version ", "\-v
 display version number
 .TP
-.BR \-\-print\-tree ", "\-p
-print the parse tree (for debugging purposes)
+.BR \-\-calc ", "\-c
+calculate the distribution
 .TP
 .BR \-\-roll ", "\-r
 roll the dice as specified. This will also be used if no other action is
@@ -25,8 +25,11 @@
 reroll many times and sum up the results to get a statistical distribution of
 values
 .TP
-.BR \-\-count ", "\-c
-specify the number of rerolls for --eval, default it 10000
+.BR \-\-count ", "\-n
+specify the number of rerolls for \-\-eval, default it 10000
+.TP
+.BR \-\-print\-tree ", "\-p
+print the parse tree (for debugging purposes)
 .TP
 .BR \-f <file>
 read the scheme description from file instead from stdin
@@ -46,7 +49,7 @@
 error. In order to get the sum of two six-sided dice, do sum(2#d6).
 
 .SH FULL SYNTAX
-.NF
+.nf
  <integer> ::= 
      -?[0-9]+
  <variable> ::=
@@ -95,7 +98,7 @@
  <input> ::=
      <expr>
      | <expr> ; <expr>
-.FI
+.fi
 
 Comments may be inserted by using double slashed (//) as in C.
 
@@ -108,7 +111,7 @@
 .PP
 .BR -
 .IP
-This is the unary minus operator. Examples: -1
+This is the unary minus operator. Examples: \-1
 .PP	
 .BR %	
 .IP
@@ -118,7 +121,7 @@
 .BR .	
 .IP
 This is the scalar concatenation operator. x . y gives xy, the 
-concatenation of x and y. Examples: -10.9, d6.d6
+concatenation of x and y. Examples: \-10.9, d6.d6
 .PP
 .BR d	
 .IP
@@ -208,7 +211,7 @@
 This is the variable assignment and substitution operator. If x is a
 variable and e and f are an expressions, then let x = e in f gives the list
 which results from evaluating f with the value of e substituted for every
-occurance of x in f. Evaluation of e is done prior to substitution.
+occurrence of x in f. Evaluation of e is done prior to substitution.
 Examples: let x = d6 in x*x
 .PP
 .BR foreach
@@ -235,78 +238,78 @@
 .SH EXAMPLES
 Count the number of dice greater than 7:
 .IP
-.NF
+.nf
 count >7 5#d10
-.FI
+.fi
 
 .PP
 Count the number of dice greater than 7 minus the number of dice equal to 1:
 .IP
-.NF
+.nf
 let c=5#d10 in (count >7 c)-(count ==1 c)
-.FI
+.fi
 
 .PP
 Count the number of rolls until a 6 is rolled:
 .IP
-.NF
+.nf
 count (while x=d6 do ((count <6 x)#d6))
-.FI
+.fi
 
 .PP
 Count the number of rolls until a 6 is rolled, more efficiently:
 .IP
-.NF
+.nf
 count (while x=(d6/6) do ((count <1 x)#(d6/6)))
-.FI
+.fi
 
 .PP
 Roll attributes for a new D&D character:
 .IP
-.NF
+.nf
 6#sum(drop low 1 4#d6)
-.FI
+.fi
 
 .PP
 Roll on the 11..66 morale check table in The Gamers' Civil War Brigade 
 Series:
 .IP
-.NF
+.nf
 d6.d6
-.FI
+.fi
 
 .PP
 Find the median of 3 d20s:
 .IP
-.NF
+.nf
 high 1 low 2 3#d20
-.FI
+.fi
 
 .PP
 3d6 with rerolls on 6s:
 .IP
-.NF
+.nf
 sum(while x=3#d6 do ((count ==6 x)#d6))
-.FI
+.fi
 
 .PP
 Roll 7 d10 and find the largest sum of identical dice:
 .IP
-.NF
+.nf
 let x = 7#d10 in high 1 (foreach y in 1..10 do sum (==y x))
-.FI
+.fi
 
 .PP
 The Fibonacci sequence is defined by Fn = Fn-1 + Fn-2, with F1 = F2 = 1.
 Calculate the first twenty Fibonacci numbers:
 .IP
-.NF
+.nf
 let n = 20 in
   let f = (1,1) in
     foreach i in 1..n do
       let f = (f,sum(high 2 f)) in
         if ==n i then f else ()
-.FI		
+.fi
 
 .PP
 Risk has battles where the attacker rolls 3d6 and the defender rolls 2d6. 
@@ -315,23 +318,23 @@
 matches, the highest wins, with ties going to the defender. The number of 
 attacker wins:
 .IP
-.NF
+.nf
 let a = 3#d6 in
   let b = 2#d6 in
     count( (<(high 1 a) high 1 b),
            (<(high 1 low 2 a) low 1 b))
-.FI		   
+.fi
 
 .PP
-Storyteller die roll with target number 8 and botches indicated at -1:
+Storyteller die roll with target number 8 and botches indicated at \-1:
 .IP
-.NF
+.nf
 let c=5#d10 in
   let succs = count >7 c in
     let ones = count ==1 c in
       if >0 succs then high 1 (0,succs-ones)
-      else if >0 ones then -1 else 0
-.FI	  
+      else if >0 ones then \-1 else 0
+.fi
 
 .PP
 Combat in Silent Death is rather complex. Three dice are rolled. If their 
@@ -343,7 +346,7 @@
 is the damage. This example assumes that the dice are two d8s and a d10, with 
 a target number of 15:
 .IP
-.NF
+.nf
 let x = 2#d8,d10 in
   (count >15 sum x)#
     let a = low 1 x in               // low die
@@ -353,7 +356,7 @@
       else if ==a <c b then c        // two low equal
       else if >a ==c b then b+c      // two high equal
 	  else b						 // all different
-.FI
+.fi
 
 .SH CREDITS
 Dicelab is based on the excellent work "roll" by Torben Mogensen (http://www.diku.dk/~torbenm/Dice.zip). Without his work and comments, this would hardly ever have happened.
