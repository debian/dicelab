%{
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#include "util.h"
#include "tree.h"

int yylex();
int yyerror(char *s);
int yyget_lineno();

char *filename = "<stdin>";

extern int cmd_parse_tree;
extern int cmd_roll;
extern int cmd_rolleval;
extern int cmd_calc;
extern int cmd_count;

int cmplist(const void *p1, const void *p2) {
	return ((struct val_list*)p1)->values[0] 
        - ((struct val_list*)p2)->values[0];
}

/*
XXX use qsort from libc, but for some reason this segfaults
void list_quicksort(struct val_list **data, int start, int end) {
	qsort(&data[start], end-start+1, sizeof(struct val_list*), cmplist);
}*/

void list_quicksort(struct val_list **data, int start, int end) {
	if (end > start) {
		int i=start-1;
		int j = end;
		struct val_list *t;
		for(;;) {
            while(cmplist(data[++i], data[end]) < 0);
            while(cmplist(data[--j >= 0 ? j : end], data[end]) > 0);
			if (i>=j)
				break;
			t = data[i];
			data[i] = data[j];
			data[j] = t;

		}
		t = data[i];
		data[i] = data[end];
		data[end] = t;

		list_quicksort(data, start, i-1);
		list_quicksort(data, i+1, end);
	}
}

struct val_list *list_sort(struct val_list *list) {
	int count = 0;
	struct val_list *cl = list;
	// count the list
	while (cl) {
		count++;
		cl = cl->next;
	}
	// create an array
	struct val_list **hash = (struct val_list**)malloc(
        sizeof(struct val_list*) * count);
	int i = 0;
	cl = list;
	while (cl) {
		hash[i] = cl;
		cl = cl->next;
		i++;
	}
	// now sort the hash
	list_quicksort(hash, 0, count-1);
	for (i = 0; i < count-1; i++) {
		hash[i]->next = hash[i+1];
	}
	hash[count-1]->next = NULL;
	cl = hash[0];
	free(hash);
	return cl;
}

%}
%error-verbose

%union {
	int ival;
	char *tval;
	expression *expr;
}

%token <ival> NUMBER
%token <tval> VARIABLE

%token IN DO WHILE ELSE
%token RANGE
%right ','
%token EQ NE LT GT LE GE
%left '+' '-' '.'
%left '*' '/' '%'
%token SUM PROD COUNT LOW HIGH
%right '#'
%right '^'
%right DICE

%token PERM SORT REV DROP KEEP FIRST LAST LET FOREACH IF THEN

%type <expr> expr
%type <expr> scalar
%type <expr> list
%type <expr> filter

%%

input: pexpr
	| input ';' pexpr
	| input ';'
	;

pexpr: expr { 
		if (cmd_parse_tree) {
			printtree($1, 0); 
		}
		if (cmd_roll) {
			int i;
			struct symtab *st = NULL;
			set_symtab($1, st);
			struct roll_value *rv = roll($1);
			for (i = 0; i < rv->count; i++) {
				printf("%i ", rv->values[i]);
			}
			printf("\n");
			free_roll(rv);
		}
		if (cmd_calc) {
			struct symtab *st = NULL;
            $1 = optimize($1);
			set_symtab($1, st);
			// we can use agnostic here as we are getting a scalar anyway.
			set_ordering($1, agnostic);
			struct val_list *vl = eval($1);
			struct val_list *cl = vl = list_sort(vl);
			while (cl) {
				if (cl->count != 1) {
					yyerror("result is not scalar");
					break;
				}
                // XXX make output resolution configurable
				printf("%3i\t%0.6f\t\n", cl->values[0], cl->prob);
				cl = cl->next;
			}
			list_free(vl);
		}
		if (cmd_rolleval) {
			int i;
			struct result_node *rl = NULL;
			for (i = 0; i < cmd_count; i++) {
				struct symtab *st = NULL;
				set_symtab($1, st);
				struct roll_value *rv = roll($1);
				if (rv->count != 1) {
					yyerror("Result not a single value");
					free_roll(rv);
					break;
				}
				result_add(&rl, rv->values[0]);
				free_roll(rv);
			}
			
			struct result_node *cl = rl;
			int sum = 0;
			while (cl != NULL) {
				sum += cl->prob;
				cl = cl->next;
			}
			cl = rl;
			while (cl != NULL) {
				float fprob;
				fprob = 1.0*cl->prob;
				fprob /= sum;
                // XXX make output resolution configurable
				printf("%3i\t%0.6f\n", cl->value, fprob);
				struct result_node *old = cl;
				cl = cl->next;
				free(old);
			}
		}
	}
	;

expr: scalar { $$ = $1; } 
	| list { $$ = $1; } 
	;

scalar: NUMBER { $$ = number_create($1); }
	| VARIABLE { $$ = variable_create($1); }
	| '(' scalar ')' { $$ = $2; }
	| '-' scalar { $$ = negate_create($2); }
	| scalar '+' scalar { $$ = plus_create($1, $3); }
	| scalar '-' scalar { $$ = minus_create($1, $3); }
	| scalar '*' scalar { $$ = multi_create($1, $3); }
	| scalar '/' scalar { $$ = divi_create($1, $3); }
	| scalar '%' scalar { $$ = mod_create($1, $3); }
	| scalar '^' scalar { $$ = expo_create($1, $3); }
	| scalar '.' scalar { $$ = scat_create($1, $3); }
	| DICE scalar { $$ = dice_create($2); }
	| SUM expr { $$ = sum_create($2); }
	| PROD expr { $$ = prod_create($2); }
	| COUNT expr { $$ = count_create($2); }
	;

list: scalar '#' expr { $$ = rep_create($1, $3); }
	| '(' list ')' { $$ = $2; }
	| '(' ')' { $$ = elist_create(); }
	| scalar RANGE scalar { $$ = range_create($1, $3); }
	| expr ',' expr { $$ = lcat_create($1, $3); }
	| PERM expr { $$ = perm_create($2); }
	| SORT expr { $$ = sort_create($2); }
	| REV expr { $$ = rev_create($2); }
	| filter { $$ = $1; }
	| IF expr THEN expr ELSE expr { $$ = ifthenelse_create($2, $4, $6); }
	| LET VARIABLE '=' expr IN expr { $$ = let_create($4, $6, $2); }
	| WHILE VARIABLE '=' expr DO expr { $$ = whiledo_create($4, $6, $2); }
	| FOREACH VARIABLE IN expr DO expr { $$ = foreach_create($4, $6, $2); }
	;

filter: DROP FIRST expr expr { $$ = first_create($3, $4, drop); }
	| KEEP FIRST expr expr { $$ = first_create($3, $4, keep); }
	| FIRST expr expr { $$ = first_create($2, $3, keep); }
	| DROP LAST expr expr { $$ = last_create($3, $4, drop); }
	| KEEP LAST expr expr { $$ = last_create($3, $4, keep); }
	| LAST expr expr { $$ = last_create($2, $3, keep); }
	| DROP HIGH expr expr { $$ = high_create($3, $4, drop); }
	| KEEP HIGH expr expr { $$ = high_create($3, $4, keep); }
	| HIGH expr expr { $$ = high_create($2, $3, keep); }
	| DROP LOW expr expr { $$ = low_create($3, $4, drop); }
	| KEEP LOW expr expr { $$ = low_create($3, $4, keep); }
	| LOW expr expr { $$ = low_create($2, $3, keep); }
	| DROP EQ expr expr { $$ = comparison_create($3, $4, drop, eq); }
	| KEEP EQ expr expr { $$ = comparison_create($3, $4, keep, eq); }
	| EQ expr expr { $$ = comparison_create($2, $3, keep, eq); }
	| DROP NE expr expr { $$ = comparison_create($3, $4, drop, ne); }
	| KEEP NE expr expr { $$ = comparison_create($3, $4, keep, ne); }
	| NE expr expr { $$ = comparison_create($2, $3, keep, ne); }
	| DROP GT expr expr { $$ = comparison_create($3, $4, drop, gt); }
	| KEEP GT expr expr { $$ = comparison_create($3, $4, keep, gt); }
	| GT expr expr { $$ = comparison_create($2, $3, keep, gt); }
	| DROP LT expr expr { $$ = comparison_create($3, $4, drop, lt); }
	| KEEP LT expr expr { $$ = comparison_create($3, $4, keep, lt); }
	| LT expr expr { $$ = comparison_create($2, $3, keep, lt); }
	| DROP GE expr expr { $$ = comparison_create($3, $4, drop, ge); }
	| KEEP GE expr expr { $$ = comparison_create($3, $4, keep, ge); }
	| GE expr expr { $$ = comparison_create($2, $3, keep, ge); }
	| DROP LE expr expr { $$ = comparison_create($3, $4, drop, le); }
	| KEEP LE expr expr { $$ = comparison_create($3, $4, keep, le); }
	| LE expr expr { $$ = comparison_create($2, $3, keep, le); }
	;
%%

int yyerror(char *s) {
	fprintf(stderr, "%s:%li %s\n", yycurrfilename(), yycurrlinenum(), s);
	return (1);
}

extern int yylineno;

long yycurrlinenum() {
	return yylineno;
}

char *yycurrfilename() {
	return filename;
}

void yynodefailed() {
	exit(1);
}
