#ifndef UTIL_H
#define UTIL_H

struct result_node {
	int value;
	int prob;
	struct result_node *next;
};

void result_add(struct result_node **list, int value);

void quicksort(int *data, int start, int end);

void reverse(int *data, int start, int end);

void permute(int *data, int count);

void all_permutations(int *data, int count, 
	void (*callback)(int *data, int count, void *arg, float farg), 
	void *arg, float farg);

long factorial(int n);

#define max(a, b) (a > b ? a : b)
#define min(a, b) (a > b ? b : a)

#endif /* UTIL_H */
