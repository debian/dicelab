/* tree.h.  Generated automatically by treecc */
#ifndef __yy_tree_h
#define __yy_tree_h
#line 4 "tree.tc"

#include  "roll.h"
#line 1 "symtab.tc"


#line 7 "eval.tc"

#include <stdlib.h>

extern float cmd_threshold;

struct val_list {
	double prob;
	int count;
	int *values;
	struct val_list *next;
};

void list_free(struct val_list *list);

#line 26 "tree.h"

#ifdef __cplusplus
extern "C" {
#endif

#define expression_kind 14
#define elist_kind 15
#define binary_kind 16
#define unary_kind 17
#define number_kind 18
#define ifthenelse_kind 38
#define variable_kind 48
#define sumrepdice_kind 49
#define sumrepany_kind 50
#define mathop_kind 27
#define scat_kind 34
#define rep_kind 35
#define range_kind 36
#define lcat_kind 37
#define filter_kind 39
#define let_kind 45
#define foreach_kind 46
#define whiledo_kind 47
#define plus_kind 28
#define minus_kind 29
#define multi_kind 30
#define divi_kind 31
#define mod_kind 32
#define expo_kind 33
#define first_kind 40
#define last_kind 41
#define high_kind 42
#define low_kind 43
#define comparison_kind 44
#define negate_kind 19
#define dice_kind 20
#define sum_kind 21
#define prod_kind 22
#define count_kind 23
#define perm_kind 24
#define sort_kind 25
#define rev_kind 26

typedef enum {
	drop,
	keep
} filter_type;

typedef struct expression__ expression;
typedef struct elist__ elist;
typedef struct binary__ binary;
typedef struct unary__ unary;
typedef struct number__ number;
typedef struct ifthenelse__ ifthenelse;
typedef struct variable__ variable;
typedef struct sumrepdice__ sumrepdice;
typedef struct sumrepany__ sumrepany;
typedef struct mathop__ mathop;
typedef struct scat__ scat;
typedef struct rep__ rep;
typedef struct range__ range;
typedef struct lcat__ lcat;
typedef struct filter__ filter;
typedef struct let__ let;
typedef struct foreach__ foreach;
typedef struct whiledo__ whiledo;
typedef struct plus__ plus;
typedef struct minus__ minus;
typedef struct multi__ multi;
typedef struct divi__ divi;
typedef struct mod__ mod;
typedef struct expo__ expo;
typedef struct first__ first;
typedef struct last__ last;
typedef struct high__ high;
typedef struct low__ low;
typedef struct comparison__ comparison;
typedef struct negate__ negate;
typedef struct dice__ dice;
typedef struct sum__ sum;
typedef struct prod__ prod;
typedef struct count__ count;
typedef struct perm__ perm;
typedef struct sort__ sort;
typedef struct rev__ rev;
typedef enum {
	agnostic,
	caring
} ordering_type;

typedef enum {
	eq,
	ne,
	lt,
	gt,
	le,
	ge
} comparison_type;


#line 1 "c_skel.h"
typedef struct
{
	struct YYNODESTATE_block *blocks__;
	struct YYNODESTATE_push *push_stack__;
	int used__;

} YYNODESTATE;
#line 135 "tree.h"
struct expression__ {
	const struct expression_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
};

struct expression_vtable__ {
	const void *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct expression_vtable__ const expression_vt__;

#define printtree(this__,depth) \
	((*(((struct expression_vtable__ *)((this__)->vtable__))->printtree_v__)) \
		((expression *)(this__), (depth)))

#define roll(this__) \
	((*(((struct expression_vtable__ *)((this__)->vtable__))->roll_v__)) \
		((expression *)(this__)))

#define set_symtab(this__,st) \
	((*(((struct expression_vtable__ *)((this__)->vtable__))->set_symtab_v__)) \
		((expression *)(this__), (st)))

#define set_ordering(this__,ordering) \
	((*(((struct expression_vtable__ *)((this__)->vtable__))->set_ordering_v__)) \
		((expression *)(this__), (ordering)))

#define eval(this__) \
	((*(((struct expression_vtable__ *)((this__)->vtable__))->eval_v__)) \
		((expression *)(this__)))

#define optimize(this__) \
	((*(((struct expression_vtable__ *)((this__)->vtable__))->optimize_v__)) \
		((expression *)(this__)))

extern void expression_set_ordering__(expression *this, ordering_type ordering);
extern expression * expression_optimize__(expression *this);

struct elist__ {
	const struct elist_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
};

struct elist_vtable__ {
	const struct expression_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct elist_vtable__ const elist_vt__;

extern void elist_printtree__(elist *this, int depth);
extern struct roll_value * elist_roll__(elist *this);
extern void elist_set_symtab__(elist *this, struct symtab * st);
extern struct val_list * elist_eval__(elist *this);

struct binary__ {
	const struct binary_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
};

struct binary_vtable__ {
	const struct expression_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct binary_vtable__ const binary_vt__;

extern void binary_set_symtab__(binary *this, struct symtab * st);
extern void binary_set_ordering__(binary *this, ordering_type ordering);
extern expression * binary_optimize__(binary *this);

struct unary__ {
	const struct unary_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr;
};

struct unary_vtable__ {
	const struct expression_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct unary_vtable__ const unary_vt__;

extern void unary_set_symtab__(unary *this, struct symtab * st);
extern void unary_set_ordering__(unary *this, ordering_type ordering);
extern expression * unary_optimize__(unary *this);

struct number__ {
	const struct number_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	int num;
};

struct number_vtable__ {
	const struct expression_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct number_vtable__ const number_vt__;

extern void number_printtree__(number *this, int depth);
extern struct roll_value * number_roll__(number *this);
extern void number_set_symtab__(number *this, struct symtab * st);
extern struct val_list * number_eval__(number *this);

struct ifthenelse__ {
	const struct ifthenelse_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * if_expr;
	expression * then_expr;
	expression * else_expr;
};

struct ifthenelse_vtable__ {
	const struct expression_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct ifthenelse_vtable__ const ifthenelse_vt__;

extern void ifthenelse_printtree__(ifthenelse *this, int depth);
extern struct roll_value * ifthenelse_roll__(ifthenelse *this);
extern void ifthenelse_set_symtab__(ifthenelse *this, struct symtab * st);
extern void ifthenelse_set_ordering__(ifthenelse *this, ordering_type ordering);
extern struct val_list * ifthenelse_eval__(ifthenelse *this);
extern expression * ifthenelse_optimize__(ifthenelse *this);

struct variable__ {
	const struct variable_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	char * varname;
};

struct variable_vtable__ {
	const struct expression_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct variable_vtable__ const variable_vt__;

extern void variable_printtree__(variable *this, int depth);
extern struct roll_value * variable_roll__(variable *this);
extern void variable_set_symtab__(variable *this, struct symtab * st);
extern void variable_set_ordering__(variable *this, ordering_type ordering);
extern struct val_list * variable_eval__(variable *this);

struct sumrepdice__ {
	const struct sumrepdice_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	struct val_list * num_dice;
	int num_sides;
};

struct sumrepdice_vtable__ {
	const struct expression_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct sumrepdice_vtable__ const sumrepdice_vt__;

extern void sumrepdice_printtree__(sumrepdice *this, int depth);
extern struct roll_value * sumrepdice_roll__(sumrepdice *this);
extern void sumrepdice_set_symtab__(sumrepdice *this, struct symtab * st);
extern struct val_list * sumrepdice_eval__(sumrepdice *this);

struct sumrepany__ {
	const struct sumrepany_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	struct val_list * number;
	struct val_list * data;
};

struct sumrepany_vtable__ {
	const struct expression_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct sumrepany_vtable__ const sumrepany_vt__;

extern void sumrepany_printtree__(sumrepany *this, int depth);
extern struct roll_value * sumrepany_roll__(sumrepany *this);
extern void sumrepany_set_symtab__(sumrepany *this, struct symtab * st);
extern struct val_list * sumrepany_eval__(sumrepany *this);

struct mathop__ {
	const struct mathop_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
};

struct mathop_vtable__ {
	const struct binary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct mathop_vtable__ const mathop_vt__;

extern void mathop_printtree__(mathop *this, int depth);
extern struct roll_value * mathop_roll__(mathop *this);
extern struct val_list * mathop_eval__(mathop *this);

struct scat__ {
	const struct scat_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
};

struct scat_vtable__ {
	const struct binary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct scat_vtable__ const scat_vt__;

extern void scat_printtree__(scat *this, int depth);
extern struct roll_value * scat_roll__(scat *this);
extern struct val_list * scat_eval__(scat *this);

struct rep__ {
	const struct rep_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
};

struct rep_vtable__ {
	const struct binary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct rep_vtable__ const rep_vt__;

extern void rep_printtree__(rep *this, int depth);
extern struct roll_value * rep_roll__(rep *this);
extern struct val_list * rep_eval__(rep *this);

struct range__ {
	const struct range_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
};

struct range_vtable__ {
	const struct binary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct range_vtable__ const range_vt__;

extern void range_printtree__(range *this, int depth);
extern struct roll_value * range_roll__(range *this);
extern struct val_list * range_eval__(range *this);

struct lcat__ {
	const struct lcat_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
};

struct lcat_vtable__ {
	const struct binary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct lcat_vtable__ const lcat_vt__;

extern void lcat_printtree__(lcat *this, int depth);
extern struct roll_value * lcat_roll__(lcat *this);
extern struct val_list * lcat_eval__(lcat *this);

struct filter__ {
	const struct filter_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
	filter_type type;
};

struct filter_vtable__ {
	const struct binary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct filter_vtable__ const filter_vt__;

struct let__ {
	const struct let_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
	char * varname;
};

struct let_vtable__ {
	const struct binary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct let_vtable__ const let_vt__;

extern void let_printtree__(let *this, int depth);
extern struct roll_value * let_roll__(let *this);
extern void let_set_ordering__(let *this, ordering_type ordering);
extern struct val_list * let_eval__(let *this);

struct foreach__ {
	const struct foreach_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
	char * varname;
};

struct foreach_vtable__ {
	const struct binary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct foreach_vtable__ const foreach_vt__;

extern void foreach_printtree__(foreach *this, int depth);
extern struct roll_value * foreach_roll__(foreach *this);
extern struct val_list * foreach_eval__(foreach *this);

struct whiledo__ {
	const struct whiledo_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
	char * varname;
};

struct whiledo_vtable__ {
	const struct binary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct whiledo_vtable__ const whiledo_vt__;

extern void whiledo_printtree__(whiledo *this, int depth);
extern struct roll_value * whiledo_roll__(whiledo *this);
extern struct val_list * whiledo_eval__(whiledo *this);

struct plus__ {
	const struct plus_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
};

struct plus_vtable__ {
	const struct mathop_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct plus_vtable__ const plus_vt__;

struct minus__ {
	const struct minus_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
};

struct minus_vtable__ {
	const struct mathop_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct minus_vtable__ const minus_vt__;

struct multi__ {
	const struct multi_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
};

struct multi_vtable__ {
	const struct mathop_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct multi_vtable__ const multi_vt__;

struct divi__ {
	const struct divi_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
};

struct divi_vtable__ {
	const struct mathop_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct divi_vtable__ const divi_vt__;

struct mod__ {
	const struct mod_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
};

struct mod_vtable__ {
	const struct mathop_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct mod_vtable__ const mod_vt__;

struct expo__ {
	const struct expo_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
};

struct expo_vtable__ {
	const struct mathop_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct expo_vtable__ const expo_vt__;

struct first__ {
	const struct first_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
	filter_type type;
};

struct first_vtable__ {
	const struct filter_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct first_vtable__ const first_vt__;

extern void first_printtree__(first *this, int depth);
extern struct roll_value * first_roll__(first *this);
extern void first_set_ordering__(first *this, ordering_type ordering);
extern struct val_list * first_eval__(first *this);

struct last__ {
	const struct last_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
	filter_type type;
};

struct last_vtable__ {
	const struct filter_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct last_vtable__ const last_vt__;

extern void last_printtree__(last *this, int depth);
extern struct roll_value * last_roll__(last *this);
extern void last_set_ordering__(last *this, ordering_type ordering);
extern struct val_list * last_eval__(last *this);

struct high__ {
	const struct high_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
	filter_type type;
};

struct high_vtable__ {
	const struct filter_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct high_vtable__ const high_vt__;

extern void high_printtree__(high *this, int depth);
extern struct roll_value * high_roll__(high *this);
extern struct val_list * high_eval__(high *this);

struct low__ {
	const struct low_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
	filter_type type;
};

struct low_vtable__ {
	const struct filter_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct low_vtable__ const low_vt__;

extern void low_printtree__(low *this, int depth);
extern struct roll_value * low_roll__(low *this);
extern struct val_list * low_eval__(low *this);

struct comparison__ {
	const struct comparison_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr1;
	expression * expr2;
	filter_type type;
	comparison_type comp;
};

struct comparison_vtable__ {
	const struct filter_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct comparison_vtable__ const comparison_vt__;

extern void comparison_printtree__(comparison *this, int depth);
extern struct roll_value * comparison_roll__(comparison *this);
extern struct val_list * comparison_eval__(comparison *this);

struct negate__ {
	const struct negate_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr;
};

struct negate_vtable__ {
	const struct unary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct negate_vtable__ const negate_vt__;

extern void negate_printtree__(negate *this, int depth);
extern struct roll_value * negate_roll__(negate *this);
extern struct val_list * negate_eval__(negate *this);

struct dice__ {
	const struct dice_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr;
};

struct dice_vtable__ {
	const struct unary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct dice_vtable__ const dice_vt__;

extern void dice_printtree__(dice *this, int depth);
extern struct roll_value * dice_roll__(dice *this);
extern struct val_list * dice_eval__(dice *this);

struct sum__ {
	const struct sum_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr;
};

struct sum_vtable__ {
	const struct unary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct sum_vtable__ const sum_vt__;

extern void sum_printtree__(sum *this, int depth);
extern struct roll_value * sum_roll__(sum *this);
extern void sum_set_ordering__(sum *this, ordering_type ordering);
extern struct val_list * sum_eval__(sum *this);
extern expression * sum_optimize__(sum *this);

struct prod__ {
	const struct prod_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr;
};

struct prod_vtable__ {
	const struct unary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct prod_vtable__ const prod_vt__;

extern void prod_printtree__(prod *this, int depth);
extern struct roll_value * prod_roll__(prod *this);
extern void prod_set_ordering__(prod *this, ordering_type ordering);
extern struct val_list * prod_eval__(prod *this);

struct count__ {
	const struct count_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr;
};

struct count_vtable__ {
	const struct unary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct count_vtable__ const count_vt__;

extern void count_printtree__(count *this, int depth);
extern struct roll_value * count_roll__(count *this);
extern void count_set_ordering__(count *this, ordering_type ordering);
extern struct val_list * count_eval__(count *this);

struct perm__ {
	const struct perm_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr;
};

struct perm_vtable__ {
	const struct unary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct perm_vtable__ const perm_vt__;

extern void perm_printtree__(perm *this, int depth);
extern struct roll_value * perm_roll__(perm *this);
extern void perm_set_ordering__(perm *this, ordering_type ordering);
extern struct val_list * perm_eval__(perm *this);

struct sort__ {
	const struct sort_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr;
};

struct sort_vtable__ {
	const struct unary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct sort_vtable__ const sort_vt__;

extern void sort_printtree__(sort *this, int depth);
extern struct roll_value * sort_roll__(sort *this);
extern void sort_set_ordering__(sort *this, ordering_type ordering);
extern struct val_list * sort_eval__(sort *this);

struct rev__ {
	const struct rev_vtable__ *vtable__;
	int kind__;
	char *filename__;
	long linenum__;
	struct symtab * symtab;
	ordering_type ordering;
	expression * expr;
};

struct rev_vtable__ {
	const struct unary_vtable__ *parent__;
	int kind__;
	const char *name__;
	void (*printtree_v__)(expression *this__, int depth);
	struct roll_value * (*roll_v__)(expression *this__);
	void (*set_symtab_v__)(expression *this__, struct symtab * st);
	void (*set_ordering_v__)(expression *this__, ordering_type ordering);
	struct val_list * (*eval_v__)(expression *this__);
	expression * (*optimize_v__)(expression *this__);
};

extern struct rev_vtable__ const rev_vt__;

extern void rev_printtree__(rev *this, int depth);
extern struct roll_value * rev_roll__(rev *this);
extern struct val_list * rev_eval__(rev *this);

extern expression *elist_create(void);
extern expression *number_create(int num);
extern expression *ifthenelse_create(expression * if_expr, expression * then_expr, expression * else_expr);
extern expression *variable_create(char * varname);
extern expression *sumrepdice_create(struct val_list * num_dice, int num_sides);
extern expression *sumrepany_create(struct val_list * number, struct val_list * data);
extern expression *scat_create(expression * expr1, expression * expr2);
extern expression *rep_create(expression * expr1, expression * expr2);
extern expression *range_create(expression * expr1, expression * expr2);
extern expression *lcat_create(expression * expr1, expression * expr2);
extern expression *let_create(expression * expr1, expression * expr2, char * varname);
extern expression *foreach_create(expression * expr1, expression * expr2, char * varname);
extern expression *whiledo_create(expression * expr1, expression * expr2, char * varname);
extern expression *plus_create(expression * expr1, expression * expr2);
extern expression *minus_create(expression * expr1, expression * expr2);
extern expression *multi_create(expression * expr1, expression * expr2);
extern expression *divi_create(expression * expr1, expression * expr2);
extern expression *mod_create(expression * expr1, expression * expr2);
extern expression *expo_create(expression * expr1, expression * expr2);
extern expression *first_create(expression * expr1, expression * expr2, filter_type type);
extern expression *last_create(expression * expr1, expression * expr2, filter_type type);
extern expression *high_create(expression * expr1, expression * expr2, filter_type type);
extern expression *low_create(expression * expr1, expression * expr2, filter_type type);
extern expression *comparison_create(expression * expr1, expression * expr2, filter_type type, comparison_type comp);
extern expression *negate_create(expression * expr);
extern expression *dice_create(expression * expr);
extern expression *sum_create(expression * expr);
extern expression *prod_create(expression * expr);
extern expression *count_create(expression * expr);
extern expression *perm_create(expression * expr);
extern expression *sort_create(expression * expr);
extern expression *rev_create(expression * expr);


#ifndef yykind
#define yykind(node__) ((node__)->kind__)
#endif

#ifndef yykindname
#define yykindname(node__) ((node__)->vtable__->name__)
#endif

#ifndef yykindof
#define yykindof(type__) (type__##_kind)
#endif

#ifndef yyisa
extern int yyisa__(const void *vtable__, int kind__);
#define yyisa(node__,type__) \
	(yyisa__((node__)->vtable__, (type__##_kind)))
#endif

#ifndef yygetfilename
#define yygetfilename(node__) ((node__)->filename__)
#endif

#ifndef yygetlinenum
#define yygetlinenum(node__) ((node__)->linenum__)
#endif

#ifndef yysetfilename
#define yysetfilename(node__, value__) \
	((node__)->filename__ = (value__))
#endif

#ifndef yysetlinenum
#define yysetlinenum(node__, value__) \
	((node__)->linenum__ = (value__))
#endif

#ifndef yytracklines_declared
extern char *yycurrfilename(void);
extern long yycurrlinenum(void);
#define yytracklines_declared 1
#endif

#ifndef yynodeops_declared
extern void yynodeinit(void);
extern void *yynodealloc(unsigned int size__);
extern int yynodepush(void);
extern void yynodepop(void);
extern void yynodeclear(void);
extern void yynodefailed(void);
#define yynodeops_declared 1
#endif

#ifdef __cplusplus
};
#endif

#endif
