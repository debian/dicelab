%{
#include "tree.h"
#include "par.h"

%}

%option noyywrap
%option nounput
%option yylineno
%%
	/* remove comments */
\/\/.*	
^[ \t]*#.*

[0-9]+	{
		yylval.ival = atoi(yytext);
		return NUMBER;
		}
	/* reserved words */
"d"		return DICE;
"sum"	return SUM;
"prod"	return PROD;
"count"	return COUNT;
"perm"	return PERM;
"sort"	return SORT;
"rev"	return REV;
"drop"	return DROP;
"keep"	return KEEP;
"low"	return LOW;
"high"	return HIGH;
"first"	return FIRST;
"last"	return LAST;
"let"	return LET;
"while"	return WHILE;
"foreach"	return FOREACH;
"in"	return IN;
"do"	return DO;
"if"	return IF;
"then"	return THEN;
"else"	return ELSE;
"=="	return EQ;
"!="	return NE;
"<="	return LE;
">="	return GE;
"<"		return LT;
">"		return GT;
".."	return RANGE;

[A-Za-z]+ {
		yylval.tval = (char*)malloc((strlen(yytext)+1) * sizeof(char));
		strcpy(yylval.tval, yytext);
		return VARIABLE;
	}
	/* eat all whitespaces */
\r\n
[[:space:]]+
	/* return other single-characters directly */
.		return(yytext[0]);
