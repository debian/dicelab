/* tree.c.  Generated automatically by treecc */
#line 8 "tree.tc"

#include <stdio.h>
#include <math.h>
#include <string.h>
#include "tree.h"
#include "util.h"

int yyerror(char *s);

#line 3 "printtree.tc"

void indent(int depth) {
	int i;
	for (i = 0; i < depth; i++) {
		printf("  ");
	}
}
#line 5 "symtab.tc"

struct symtab {
	char *name;
	union {
		struct roll_value *rvalue;  // used by summing up
		struct val_list *lvalue; // used by calculations 
		ordering_type ordering; // used to defer the ordering for a variable
	};
	struct symtab *next;
};
#line 23 "eval.tc"

struct val_list *list_new(size_t count, double prob) {
	struct val_list *ret = (struct val_list*)malloc(sizeof(struct val_list));
	ret->next = NULL;
	ret->prob = prob;
	ret->count = count;
	ret->values = count ? (int*)malloc(count * sizeof(int)): NULL;
    memset(ret->values, 0, count * sizeof(int));
	return ret;
}

// add cret to ret
void list_add(struct val_list **ret, struct val_list *cret, ordering_type
ordering) {
	// and add to the list
	struct val_list *search = *ret;
	struct val_list *lsearch = NULL;
	if (ordering == agnostic) {
		quicksort(cret->values, 0, cret->count-1);
	}
	while (search) {
		if ((search->count == cret->count) &&
			(memcmp(search->values, cret->values, 
				cret->count * sizeof(int)) == 0)) {
			break;
		}
		lsearch = search;
		search = search->next;
	}
	if (search) {
		// already present, add
		search->prob += cret->prob;
		free(cret->values);
		free(cret);
	}
	else if (lsearch) {
		lsearch->next = cret;
	}
	else {
		*ret = cret;
	}
}

struct val_list *error_val() {
	struct val_list *ret = list_new(1, 1.0);
	ret->values[0] = 0;
	return ret;
}

void list_free(struct val_list *list) {
	while (list != NULL) {
		struct val_list *cur = list; 
		list = list->next;
		free(cur->values);
		free(cur);
	}
}
#line 770 "eval.tc"

void rec_whiledo(struct val_list *v, char *varname, struct symtab *symtab, 
		expression *expr, int *data, int count, struct val_list **ret,
		ordering_type ordering) {
	struct val_list *cv = v;
	while (cv) {
		// create a current list
		int ccount = count + cv->count;
		int *cdata = (int*)malloc(ccount * sizeof(int));
		memcpy(cdata, data, count * sizeof(int));
		memcpy(&cdata[count], cv->values, cv->count * sizeof(int));
		// do this recursion
		if ((cv->count != 0) && (cv->prob >= cmd_threshold)) {
			// set the variable
			struct symtab *nst = (struct symtab*)malloc(sizeof(struct symtab));
			nst->name = varname;
			nst->rvalue = (struct roll_value*)malloc(sizeof(struct roll_value));
			nst->rvalue->count = cv->count;
			nst->rvalue->values = (int*)malloc(sizeof(int) *  cv->count);
			memcpy(nst->rvalue->values, cv->values, sizeof(int) * cv->count);
			nst->next = symtab;
			set_symtab(expr, nst);
			// call the function
			struct val_list *x = eval(expr);
			// adjust the probability
			struct val_list *cx = x;
			while (cx) {
				cx->prob *= cv->prob;
				cx = cx->next;
			}
			// recursive call
			rec_whiledo(x, varname, symtab, expr, cdata, ccount, ret, ordering);
			// free the data, symtab and list
			free(cdata);
			free_roll(nst->rvalue);
			free(nst);
			list_free(x);
		}
		else {
			// add to final list
			struct val_list *cur = list_new(ccount, cv->prob);
			free(cur->values);
			cur->values = cdata;
			list_add(ret, cur, ordering);
		}
		// next element
		cv = cv->next;
	}
}
#line 937 "eval.tc"

void cb_perm(int *data, int count, void *arg, float farg) {
	struct val_list **pret = (struct val_list**)arg;
	struct val_list *current = list_new(count, farg);
	memcpy(current->values, data, current->count * sizeof(int));
	// XXX pass this from the tree instead of using caring all the time
	// or even better, don't do the permutations if we don't care about the
	// ordering
	list_add(pret, current, caring);
}
#line 3 "optimize.tc"


#include <values.h>

#ifndef MINFLOAT
#define MINFLOAT	0.0000001
#endif

// this is taken from http://en.wikipedia.org/wiki/Dice
// calculates the probability for value "pos" in the sum of "num" 
// dice with "sides" faces
double Fsumdice(double *cache, int mnum, int num, int sides, int pos) {
	if (pos > ((num*sides-num)/2)+num) {
		pos = num*sides - pos + num;
	}

	if (num == 1) {
		if ((pos < 1) || (pos > sides)) {
			return 0.0;
		}
		return 1.0/sides;
	}
	else {
		if (pos < num) {
			return 0.0;
		}
		int i;
		if (cache[mnum*(pos-num)+num] < -0.5) {
			double ret = 0;
			for (i = 1; i < min(pos, sides+1); i++) {
				ret += Fsumdice(cache, mnum, 1, sides, i) 
					* Fsumdice(cache, mnum, num-1, sides, pos-i); 
			}
			cache[mnum*(pos-num)+num] = ret;
			return ret;
		}
		return cache[mnum*(pos-num)+num];
	}
}

// calculates the probability that the sum of "num" rolls of 
// distribution "dist" have the value "pos"
double Fsumany(double *cache, int mnum, int minval, int maxval, int num,
        struct val_list *dist, int pos) {
    struct val_list *cdist = dist;
    if (num == 1) {
        while (cdist) {
            if (cdist->values[0] == pos) {
                return cdist->prob;
            }
            cdist = cdist->next;
        }
        return 0.0;
    }
    else {
        int cpos = pos*mnum+num;
        if ((pos < minval * num) || (pos > maxval * pos)) {
            return 0.0;
        }
        if (cache[cpos] < -0.5) {
		    double ret = 0.0;
            while (cdist) {
                ret += cdist->prob * Fsumany(cache, mnum, minval, maxval,
                    num - 1, dist, pos - cdist->values[0]);
                cdist = cdist->next;
            }
            cache[cpos] = ret;
            return ret;
        }
        return cache[cpos];
    }
}

#line 225 "tree.c"

#define YYNODESTATE_TRACK_LINES 1
#line 1 "c_skel.c"
/*
 * treecc node allocation routines for C.
 *
 * Copyright (C) 2001  Southern Storm Software, Pty Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * As a special exception, when this file is copied by treecc into
 * a treecc output file, you may use that output file without restriction.
 */

#include <stdlib.h>

#ifndef YYNODESTATE_BLKSIZ
#define	YYNODESTATE_BLKSIZ	2048
#endif

/*
 * Types used by the allocation routines.
 */
struct YYNODESTATE_block
{
	char data__[YYNODESTATE_BLKSIZ];
	struct YYNODESTATE_block *next__;

};
struct YYNODESTATE_push
{
	struct YYNODESTATE_push *next__;
	struct YYNODESTATE_block *saved_block__;
	int saved_used__;
};

/*
 * The fixed global state to use for non-reentrant allocation.
 */
#ifndef YYNODESTATE_REENTRANT
static YYNODESTATE fixed_state__;
#endif

/*
 * Some macro magic to determine the default alignment
 * on this machine.  This will compile down to a constant.
 */
#define	YYNODESTATE_ALIGN_CHECK_TYPE(type,name)	\
	struct _YYNODESTATE_align_##name { \
		char pad; \
		type field; \
	}
#define	YYNODESTATE_ALIGN_FOR_TYPE(type)	\
	((unsigned)(&(((struct _YYNODESTATE_align_##type *)0)->field)))
#define	YYNODESTATE_ALIGN_MAX(a,b)	\
	((a) > (b) ? (a) : (b))
#define	YYNODESTATE_ALIGN_MAX3(a,b,c) \
	(YYNODESTATE_ALIGN_MAX((a), YYNODESTATE_ALIGN_MAX((b), (c))))
YYNODESTATE_ALIGN_CHECK_TYPE(int, int);
YYNODESTATE_ALIGN_CHECK_TYPE(long, long);
#if defined(WIN32) && !defined(__CYGWIN__)
YYNODESTATE_ALIGN_CHECK_TYPE(__int64, long_long);
#else
YYNODESTATE_ALIGN_CHECK_TYPE(long long, long_long);
#endif
YYNODESTATE_ALIGN_CHECK_TYPE(void *, void_p);
YYNODESTATE_ALIGN_CHECK_TYPE(float, float);
YYNODESTATE_ALIGN_CHECK_TYPE(double, double);
#define	YYNODESTATE_ALIGNMENT	\
	YYNODESTATE_ALIGN_MAX( \
			YYNODESTATE_ALIGN_MAX3	\
			(YYNODESTATE_ALIGN_FOR_TYPE(int), \
		     YYNODESTATE_ALIGN_FOR_TYPE(long), \
			 YYNODESTATE_ALIGN_FOR_TYPE(long_long)), \
  	     YYNODESTATE_ALIGN_MAX3 \
		 	(YYNODESTATE_ALIGN_FOR_TYPE(void_p), \
			 YYNODESTATE_ALIGN_FOR_TYPE(float), \
			 YYNODESTATE_ALIGN_FOR_TYPE(double)))

/*
 * Initialize the node allocation pool.
 */
#ifdef YYNODESTATE_REENTRANT
void yynodeinit(state__)
YYNODESTATE *state__;
{
#else
void yynodeinit()
{
	YYNODESTATE *state__ = &fixed_state__;
#endif
	state__->blocks__ = 0;
	state__->push_stack__ = 0;
	state__->used__ = 0;
}

/*
 * Allocate a block of memory.
 */
#ifdef YYNODESTATE_REENTRANT
void *yynodealloc(state__, size__)
YYNODESTATE *state__;
unsigned int size__;
{
#else
void *yynodealloc(size__)
unsigned int size__;
{
	YYNODESTATE *state__ = &fixed_state__;
#endif
	struct YYNODESTATE_block *block__;
	void *result__;

	/* Round the size to the next alignment boundary */
	size__ = (size__ + YYNODESTATE_ALIGNMENT - 1) &
				~(YYNODESTATE_ALIGNMENT - 1);

	/* Do we need to allocate a new block? */
	block__ = state__->blocks__;
	if(!block__ || (state__->used__ + size__) > YYNODESTATE_BLKSIZ)
	{
		if(size__ > YYNODESTATE_BLKSIZ)
		{
			/* The allocation is too big for the node pool */
			return (void *)0;
		}
		block__ = (struct YYNODESTATE_block *)
						malloc(sizeof(struct YYNODESTATE_block));
		if(!block__)
		{
			/* The system is out of memory.  The programmer can
			   supply the "yynodefailed" function to report the
			   out of memory state and/or abort the program */
#ifdef YYNODESTATE_REENTRANT
			yynodefailed(state__);
#else
			yynodefailed();
#endif
			return (void *)0;
		}
		block__->next__ = state__->blocks__;
		state__->blocks__ = block__;
		state__->used__ = 0;
	}

	/* Allocate the memory and return it */
	result__ = (void *)(block__->data__ + state__->used__);
	state__->used__ += size__;
	return result__;
}

/*
 * Push the node allocation state.
 */
#ifdef YYNODESTATE_REENTRANT
int yynodepush(state__)
YYNODESTATE *state__;
{
#else
int yynodepush()
{
	YYNODESTATE *state__ = &fixed_state__;
#endif
	struct YYNODESTATE_block *saved_block__;
	int saved_used__;
	struct YYNODESTATE_push *push_item__;

	/* Save the current state of the node allocation pool */
	saved_block__ = state__->blocks__;
	saved_used__ = state__->used__;

	/* Allocate space for a push item */
#ifdef YYNODESTATE_REENTRANT
	push_item__ = (struct YYNODESTATE_push *)
			yynodealloc(state__, sizeof(struct YYNODESTATE_push));
#else
	push_item__ = (struct YYNODESTATE_push *)
			yynodealloc(sizeof(struct YYNODESTATE_push));
#endif
	if(!push_item__)
	{
		return 0;
	}

	/* Copy the saved information to the push item */
	push_item__->saved_block__ = saved_block__;
	push_item__->saved_used__ = saved_used__;

	/* Add the push item to the push stack */
	push_item__->next__ = state__->push_stack__;
	state__->push_stack__ = push_item__;
	return 1;
}

/*
 * Pop the node allocation state.
 */
#ifdef YYNODESTATE_REENTRANT
void yynodepop(state__)
YYNODESTATE *state__;
{
#else
void yynodepop()
{
	YYNODESTATE *state__ = &fixed_state__;
#endif
	struct YYNODESTATE_push *push_item__;
	struct YYNODESTATE_block *saved_block__;
	struct YYNODESTATE_block *temp_block__;

	/* Pop the top of the push stack */
	push_item__ = state__->push_stack__;
	if(push_item__ == 0)
	{
		saved_block__ = 0;
		state__->used__ = 0;
	}
	else
	{
		saved_block__ = push_item__->saved_block__;
		state__->used__ = push_item__->saved_used__;
		state__->push_stack__ = push_item__->next__;
	}

	/* Free unnecessary blocks */
	while(state__->blocks__ != saved_block__)
	{
		temp_block__ = state__->blocks__;
		state__->blocks__ = temp_block__->next__;
		free(temp_block__);
	}
}

/*
 * Clear the node allocation pool completely.
 */
#ifdef YYNODESTATE_REENTRANT
void yynodeclear(state__)
YYNODESTATE *state__;
{
#else
void yynodeclear()
{
	YYNODESTATE *state__ = &fixed_state__;
#endif
	struct YYNODESTATE_block *temp_block__;
	while(state__->blocks__ != 0)
	{
		temp_block__ = state__->blocks__;
		state__->blocks__ = temp_block__->next__;
		free(temp_block__);
	}
	state__->push_stack__ = 0;
	state__->used__ = 0;
}
#line 493 "tree.c"
void expression_set_ordering__(expression *this, ordering_type ordering)
#line 7 "ordering.tc"
{
	this->ordering = ordering;
}
#line 499 "tree.c"

expression * expression_optimize__(expression *this)
#line 225 "optimize.tc"
{
	return this;
}
#line 506 "tree.c"

struct expression_vtable__ const expression_vt__ = {
	0,
	expression_kind,
	"expression",
	(void (*)(expression *this__, int depth))0,
	(struct roll_value * (*)(expression *this__))0,
	(void (*)(expression *this__, struct symtab * st))0,
	(void (*)(expression *this__, ordering_type ordering))expression_set_ordering__,
	(struct val_list * (*)(expression *this__))0,
	(expression * (*)(expression *this__))expression_optimize__,
};

void elist_printtree__(elist *this, int depth)
#line 13 "printtree.tc"
{
	indent(depth);
	printf("empty list\n");
}
#line 526 "tree.c"

struct roll_value * elist_roll__(elist *this)
#line 4 "roll.tc"
{
	struct roll_value *rv = new_roll_single(0);
	rv->count = 0;
	return rv;
}
#line 535 "tree.c"

void elist_set_symtab__(elist *this, struct symtab * st)
#line 20 "symtab.tc"
{
}
#line 541 "tree.c"

struct val_list * elist_eval__(elist *this)
#line 83 "eval.tc"
{
	return list_new(0, 1.0);
}
#line 548 "tree.c"

struct elist_vtable__ const elist_vt__ = {
	&expression_vt__,
	elist_kind,
	"elist",
	(void (*)(expression *this__, int depth))elist_printtree__,
	(struct roll_value * (*)(expression *this__))elist_roll__,
	(void (*)(expression *this__, struct symtab * st))elist_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))expression_set_ordering__,
	(struct val_list * (*)(expression *this__))elist_eval__,
	(expression * (*)(expression *this__))expression_optimize__,
};

void binary_set_symtab__(binary *this, struct symtab * st)
#line 39 "symtab.tc"
{
	this->symtab = st;
	set_symtab(this->expr1, st);
	set_symtab(this->expr2, st);
}
#line 569 "tree.c"

void binary_set_ordering__(binary *this, ordering_type ordering)
#line 18 "ordering.tc"
{
	this->ordering = ordering;
	set_ordering(this->expr1, ordering);
	set_ordering(this->expr2, ordering);
}
#line 578 "tree.c"

expression * binary_optimize__(binary *this)
#line 230 "optimize.tc"
{
	this->expr1 = optimize(this->expr1);
	this->expr2 = optimize(this->expr2);
	return (expression*)this;
}
#line 587 "tree.c"

struct binary_vtable__ const binary_vt__ = {
	&expression_vt__,
	binary_kind,
	"binary",
	(void (*)(expression *this__, int depth))0,
	(struct roll_value * (*)(expression *this__))0,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))0,
	(expression * (*)(expression *this__))binary_optimize__,
};

void unary_set_symtab__(unary *this, struct symtab * st)
#line 33 "symtab.tc"
{
	this->symtab = st;
	set_symtab(this->expr, st);
}
#line 607 "tree.c"

void unary_set_ordering__(unary *this, ordering_type ordering)
#line 12 "ordering.tc"
{
	this->ordering = ordering;
	set_ordering(this->expr, ordering);
}
#line 615 "tree.c"

expression * unary_optimize__(unary *this)
#line 237 "optimize.tc"
{
	this->expr = optimize(this->expr);
	return (expression*)this;
}
#line 623 "tree.c"

struct unary_vtable__ const unary_vt__ = {
	&expression_vt__,
	unary_kind,
	"unary",
	(void (*)(expression *this__, int depth))0,
	(struct roll_value * (*)(expression *this__))0,
	(void (*)(expression *this__, struct symtab * st))unary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))unary_set_ordering__,
	(struct val_list * (*)(expression *this__))0,
	(expression * (*)(expression *this__))unary_optimize__,
};

void number_printtree__(number *this, int depth)
#line 19 "printtree.tc"
{
	indent(depth);
	printf("%i\n", this->num);
}
#line 643 "tree.c"

struct roll_value * number_roll__(number *this)
#line 11 "roll.tc"
{
	struct roll_value *rv = new_roll_single(this->num);
	return rv;
}
#line 651 "tree.c"

void number_set_symtab__(number *this, struct symtab * st)
#line 24 "symtab.tc"
{
}
#line 657 "tree.c"

struct val_list * number_eval__(number *this)
#line 1005 "eval.tc"
{
	struct val_list *ret = list_new(1, 1.0);
	ret->values[0] = this->num;
	return ret;
}
#line 666 "tree.c"

struct number_vtable__ const number_vt__ = {
	&expression_vt__,
	number_kind,
	"number",
	(void (*)(expression *this__, int depth))number_printtree__,
	(struct roll_value * (*)(expression *this__))number_roll__,
	(void (*)(expression *this__, struct symtab * st))number_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))expression_set_ordering__,
	(struct val_list * (*)(expression *this__))number_eval__,
	(expression * (*)(expression *this__))expression_optimize__,
};

void ifthenelse_printtree__(ifthenelse *this, int depth)
#line 241 "printtree.tc"
{
	indent(depth);
	printf("if\n");
	printtree(this->if_expr, depth+1);
	indent(depth);
	printf("then\n");
	printtree(this->then_expr, depth+1);
	indent(depth);
	printf("else\n");
	printtree(this->else_expr, depth+1);
}
#line 693 "tree.c"

struct roll_value * ifthenelse_roll__(ifthenelse *this)
#line 486 "roll.tc"
{
	struct roll_value *if_expr = roll(this->if_expr);
	if (if_expr->count > 0) {
		free_roll(if_expr);
		return roll(this->then_expr);
	}
	else {
		free_roll(if_expr);
		return roll(this->else_expr);
	}
}
#line 708 "tree.c"

void ifthenelse_set_symtab__(ifthenelse *this, struct symtab * st)
#line 46 "symtab.tc"
{
	this->symtab = st;
	set_symtab(this->if_expr, st);
	set_symtab(this->then_expr, st);
	set_symtab(this->else_expr, st);
}
#line 718 "tree.c"

void ifthenelse_set_ordering__(ifthenelse *this, ordering_type ordering)
#line 69 "ordering.tc"
{
	this->ordering = ordering;
    set_ordering(this->if_expr, agnostic);
    set_ordering(this->then_expr, ordering);
	set_ordering(this->else_expr, ordering);
}
#line 728 "tree.c"

struct val_list * ifthenelse_eval__(ifthenelse *this)
#line 1012 "eval.tc"
{
	struct val_list *cnt = eval(this->if_expr);
	struct val_list *ccnt = cnt;
	struct val_list *ret = NULL;

	while (ccnt) {
		struct val_list *cur;
		if (ccnt->count > 0) {
			cur = eval(this->then_expr);
		}
		else {
			cur = eval(this->else_expr);
		}
		struct val_list *temp;
		while (cur) {
			temp = cur;
			cur = cur->next;
			temp->next = NULL;
			temp->prob *= ccnt->prob;
			list_add(&ret, temp, this->ordering);
		}
		ccnt = ccnt->next;
	}
	list_free(cnt);
	return ret;
}
#line 758 "tree.c"

expression * ifthenelse_optimize__(ifthenelse *this)
#line 243 "optimize.tc"
{
	this->if_expr = optimize(this->if_expr);
	this->then_expr = optimize(this->then_expr);
	this->else_expr = optimize(this->else_expr);
	return (expression*)this;
}
#line 768 "tree.c"

struct ifthenelse_vtable__ const ifthenelse_vt__ = {
	&expression_vt__,
	ifthenelse_kind,
	"ifthenelse",
	(void (*)(expression *this__, int depth))ifthenelse_printtree__,
	(struct roll_value * (*)(expression *this__))ifthenelse_roll__,
	(void (*)(expression *this__, struct symtab * st))ifthenelse_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))ifthenelse_set_ordering__,
	(struct val_list * (*)(expression *this__))ifthenelse_eval__,
	(expression * (*)(expression *this__))ifthenelse_optimize__,
};

void variable_printtree__(variable *this, int depth)
#line 25 "printtree.tc"
{
	indent(depth);
	printf("%s\n", this->varname);
}
#line 788 "tree.c"

struct roll_value * variable_roll__(variable *this)
#line 514 "roll.tc"
{
	struct symtab *nst = this->symtab;
	struct roll_value *ret = NULL;
	while (nst != NULL) {
		if (strcmp(this->varname, nst->name) == 0) {
			/* copy the roll value */
			ret = (struct roll_value*)malloc(sizeof(struct roll_value));
			ret->values = (int*)malloc(sizeof(int) * nst->rvalue->count);
			memcpy(ret->values, nst->rvalue->values, 
				sizeof(int) * nst->rvalue->count);
			ret->count = nst->rvalue->count;
			break;
		}
		nst = nst->next;
	}
	if (ret == NULL) {
		char *emsg = (char*)malloc(sizeof(char)*22+strlen(this->varname));
		sprintf(emsg, "Variable \"%s\" not found", this->varname);
		yyerror(emsg);
		free(emsg);
		ret = (struct roll_value*)malloc(sizeof(struct roll_value));
		ret->values = (int*)malloc(sizeof(int) * 1);
		ret->values[0] = 0;
		ret->count = 1;
	}
	return ret;
}
#line 819 "tree.c"

void variable_set_symtab__(variable *this, struct symtab * st)
#line 28 "symtab.tc"
{
	this->symtab = st;
}
#line 826 "tree.c"

void variable_set_ordering__(variable *this, ordering_type ordering)
#line 77 "ordering.tc"
{
	this->ordering = ordering;
	// get the symtab
	struct symtab *nst = this->symtab;
	while (nst != NULL) {
		if (strcmp(this->varname, nst->name) == 0) {
			break;
		}
		nst = nst->next;
	}

	if (nst && nst->ordering == agnostic) {
		nst->ordering = ordering;
	}
}
#line 845 "tree.c"

struct val_list * variable_eval__(variable *this)
#line 1040 "eval.tc"
{
	struct symtab *nst = this->symtab;
	struct val_list *ret = NULL;
	while (nst != NULL) {
		if (strcmp(this->varname, nst->name) == 0) {
			ret = list_new(nst->rvalue->count, 1.0);
			memcpy(ret->values, nst->rvalue->values, sizeof(int) * nst->rvalue->count);
			break;
		}
		nst = nst->next;
	}
	if (ret == NULL) {
		char *emsg = (char*)malloc(sizeof(char)*22+strlen(this->varname));
		sprintf(emsg, "Variable \"%s\" not found", this->varname);
		yyerror(emsg);
		free(emsg);
		ret = error_val();
	}
	return ret;
}
#line 869 "tree.c"

struct variable_vtable__ const variable_vt__ = {
	&expression_vt__,
	variable_kind,
	"variable",
	(void (*)(expression *this__, int depth))variable_printtree__,
	(struct roll_value * (*)(expression *this__))variable_roll__,
	(void (*)(expression *this__, struct symtab * st))variable_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))variable_set_ordering__,
	(struct val_list * (*)(expression *this__))variable_eval__,
	(expression * (*)(expression *this__))expression_optimize__,
};

void sumrepdice_printtree__(sumrepdice *this, int depth)
#line 89 "optimize.tc"
{
	// unsupported, only used in eval
}
#line 888 "tree.c"

struct roll_value * sumrepdice_roll__(sumrepdice *this)
#line 85 "optimize.tc"
{
	// unsupported, only used in eval
}
#line 895 "tree.c"

void sumrepdice_set_symtab__(sumrepdice *this, struct symtab * st)
#line 93 "optimize.tc"
{
	// not needed, don't use symtab
}
#line 902 "tree.c"

struct val_list * sumrepdice_eval__(sumrepdice *this)
#line 97 "optimize.tc"
{
	int n;
	struct val_list *ret = NULL;
	struct val_list *cnum = this->num_dice;
	while (cnum) {
		if (cnum->count != 1) {
			yyerror("Argument 1 to sumrep operator is not scalar");
			list_free(this->num_dice);
			list_free(ret);
			return error_val();
		}
		int num_dice_val = cnum->values[0];
        if (num_dice_val > 0) {
            int expected = num_dice_val*
                ((num_dice_val*this->num_sides-num_dice_val)/2)
                +num_dice_val+1;
            double *cache = malloc(sizeof(double) * expected);
            for (n = 0; n < expected; n++) {
                cache[n] = -1.0;
            }
            for (n = num_dice_val * this->num_sides; 
                    n >= num_dice_val; n--) {
                struct val_list *cret = 
                    (struct val_list*)malloc(sizeof(struct val_list));
                cret->next = NULL;
                cret->count = 1;
                cret->prob = Fsumdice(cache, num_dice_val, num_dice_val, 
                    this->num_sides, n) * cnum->prob;
                cret->values = (int*)malloc(sizeof(int));
                cret->values[0] = n;
                list_add(&ret, cret, this->ordering);
            } 
            free(cache);
        }
		cnum = cnum->next;
	}
	if (ret == NULL) {
		return list_new(1, 1.0);
	}
	return ret;
}
#line 947 "tree.c"

struct sumrepdice_vtable__ const sumrepdice_vt__ = {
	&expression_vt__,
	sumrepdice_kind,
	"sumrepdice",
	(void (*)(expression *this__, int depth))sumrepdice_printtree__,
	(struct roll_value * (*)(expression *this__))sumrepdice_roll__,
	(void (*)(expression *this__, struct symtab * st))sumrepdice_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))expression_set_ordering__,
	(struct val_list * (*)(expression *this__))sumrepdice_eval__,
	(expression * (*)(expression *this__))expression_optimize__,
};

void sumrepany_printtree__(sumrepany *this, int depth)
#line 149 "optimize.tc"
{
	// unsupported, only used in eval
}
#line 966 "tree.c"

struct roll_value * sumrepany_roll__(sumrepany *this)
#line 145 "optimize.tc"
{
	// unsupported, only used in eval
}
#line 973 "tree.c"

void sumrepany_set_symtab__(sumrepany *this, struct symtab * st)
#line 153 "optimize.tc"
{
	// not needed, don't use symtab
}
#line 980 "tree.c"

struct val_list * sumrepany_eval__(sumrepany *this)
#line 157 "optimize.tc"
{
	int n;
	struct val_list *ret = NULL;
	struct val_list *cnum = this->number;
    int minval = -1;
    int maxval = 0;
    struct val_list *cval = this->data;
    while (cval) {
        if ((minval == -1) || (cval->values[0] < minval)) {
            minval = cval->values[0];
        }
        if (cval->values[0] > maxval) {
            maxval = cval->values[0];
        }
        cval = cval->next;
    }
	while (cnum) {
		if (cnum->count != 1) {
			yyerror("Argument 1 to sumrep operator is not scalar");
			list_free(this->number);
			list_free(this->data);
			list_free(ret);
			return error_val();
		}
        if (cnum->values[0] == 0) {
            struct val_list *cret = 
                (struct val_list*)malloc(sizeof(struct val_list));
            cret->next = NULL;
            cret->count = 1;
            cret->prob = cnum->prob;
            cret->values = (int*)malloc(sizeof(int));
            cret->values[0] = 0;
            list_add(&ret, cret, this->ordering);
        }
        else {
            int expected = (cnum->values[0] * maxval + 1) * cnum->values[0] + 1;
            double *cache = malloc(sizeof(double) * expected);
            for (n = 0; n < expected; n++) {
                 cache[n] = -1.0;
            }
            for (n = cnum->values[0] * minval; n <= cnum->values[0] * maxval; n++) {
                struct val_list *cret = 
                    (struct val_list*)malloc(sizeof(struct val_list));
                cret->next = NULL;
                cret->count = 1;
                cret->prob = Fsumany(cache, cnum->values[0], minval, maxval,
                    cnum->values[0], this->data, n) * cnum->prob;
                cret->values = (int*)malloc(sizeof(int));
                cret->values[0] = n;
                if (cret->prob > MINFLOAT) { 
                    list_add(&ret, cret, this->ordering);
                }
                else {
                    list_free(cret);    
                }
            }
            free(cache);
        }
		cnum = cnum->next;
    }
	if (ret == NULL) {
		return list_new(1, 1.0);
	}
	return ret;
}
#line 1049 "tree.c"

struct sumrepany_vtable__ const sumrepany_vt__ = {
	&expression_vt__,
	sumrepany_kind,
	"sumrepany",
	(void (*)(expression *this__, int depth))sumrepany_printtree__,
	(struct roll_value * (*)(expression *this__))sumrepany_roll__,
	(void (*)(expression *this__, struct symtab * st))sumrepany_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))expression_set_ordering__,
	(struct val_list * (*)(expression *this__))sumrepany_eval__,
	(expression * (*)(expression *this__))expression_optimize__,
};

void mathop_printtree__(mathop *this, int depth)
#line 38 "printtree.tc"
{
	indent(depth);
	switch (yykind(this)) {
		case plus_kind:
			printf("+\n");
			break;
		case minus_kind:
			printf("-\n");
			break;
		case multi_kind:
			printf("*\n");
			break;
		case divi_kind:
			printf("/\n");
			break;
		case mod_kind:
			printf("%%\n");
			break;
		case expo_kind:
			printf("^\n");
			break;
		default:
			yyerror("Unknown math op");
			
	}
	printtree(this->expr1, depth+1);
	printtree(this->expr2, depth+1);
}
#line 1093 "tree.c"

struct roll_value * mathop_roll__(mathop *this)
#line 17 "roll.tc"
{
	struct roll_value *rv1 = roll(this->expr1);
	struct roll_value *rv2 = roll(this->expr2);
	if (rv1->count != 1) {
		yyerror("Argument 1 to math op isn't scalar");
		free_roll(rv2);
		rv1->count = 1;
		rv1->values[0] = 0;
		return rv1;
	}
	if (rv2->count != 1) {
		yyerror("Argument 2 to math op isn't scalar");
		free_roll(rv2);
		rv1->count = 1;
		rv1->values[0] = 0;
		return rv1;
	}
	switch (yykind(this)) {
		case plus_kind:
			rv1->values[0] += rv2->values[0];
			break;
		case minus_kind:
			rv1->values[0] -= rv2->values[0];
			break;
		case multi_kind:
			rv1->values[0] *= rv2->values[0];
			break;
		case divi_kind:
			rv1->values[0] /= rv2->values[0];
			break;
		case mod_kind:
			rv1->values[0] %= rv2->values[0];
			break;
		case expo_kind:
			rv1->values[0] = (int) pow(rv1->values[0], rv2->values[0]);
			break;
		default:
			yyerror("Unknown math op");
			
	}

	free_roll(rv2);
	return rv1;
}
#line 1141 "tree.c"

struct val_list * mathop_eval__(mathop *this)
#line 88 "eval.tc"
{
	struct val_list *e1 = eval(this->expr1);
	struct val_list *e2 = eval(this->expr2);
	struct val_list *ret = NULL;
	struct val_list *ce1 = e1;
	struct val_list *ce2;
	
	while (ce1) {
		ce2 = e2;
		while (ce2) {
			if (ce1->count != 1) {
				yyerror("Argument 1 to math operator is not scalar");
				list_free(e1);
				list_free(e2);
				return error_val();
			}
			if (ce2->count != 1) {
				yyerror("Argument 2 to math operator is not scalar");
				list_free(e1);
				list_free(e2);
				return error_val();
			}
			struct val_list *cret = list_new(1, ce1->prob * ce2->prob);
			switch (yykind(this)) {
				case plus_kind:
					cret->values[0] = ce1->values[0] + ce2->values[0];
					break;
				case minus_kind:
					cret->values[0] = ce1->values[0] - ce2->values[0];
					break;
				case multi_kind:
					cret->values[0] = ce1->values[0] * ce2->values[0];
					break;
				case divi_kind:
					cret->values[0] = ce1->values[0] / ce2->values[0];
					break;
				case mod_kind:
					cret->values[0] = ce1->values[0] % ce2->values[0];
					break;
				case expo_kind:
					cret->values[0] = (int) pow(ce1->values[0], ce2->values[0]);
					break;
				default:
					yyerror("Unknown math op");
					list_free(e1);
					list_free(e2);
					return error_val();
			}
			list_add(&ret, cret, this->ordering);
			ce2 = ce2->next;
		}
		ce1 = ce1->next;
	}
	list_free(e1);
	list_free(e2);
	return ret;
}
#line 1202 "tree.c"

struct mathop_vtable__ const mathop_vt__ = {
	&binary_vt__,
	mathop_kind,
	"mathop",
	(void (*)(expression *this__, int depth))mathop_printtree__,
	(struct roll_value * (*)(expression *this__))mathop_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))mathop_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

void scat_printtree__(scat *this, int depth)
#line 68 "printtree.tc"
{
	indent(depth);
	printf(".\n");
	printtree(this->expr1, depth+1);
	printtree(this->expr2, depth+1);
}
#line 1224 "tree.c"

struct roll_value * scat_roll__(scat *this)
#line 64 "roll.tc"
{
	struct roll_value *rv1 = roll(this->expr1);
	struct roll_value *rv2 = roll(this->expr2);
	if (rv1->count != 1) {
		yyerror("Argument 1 to scat (.) isn't scalar");
		free_roll(rv2);
		rv1->count = 1;
		rv1->values[0] = 0;
		return rv1;
	}
	if (rv2->count != 1) {
		yyerror("Argument 2 to scat (.) isn't scalar");
		free_roll(rv2);
		rv1->count = 1;
		rv1->values[0] = 0;
		return rv1;
	}
	int i = 10;
	while (i < rv2->values[0]) {
		i *= 10;
	}
	rv1->values[0] *= i;
	rv1->values[0] += rv2->values[0];
	free_roll(rv2);
	return rv1;
}
#line 1254 "tree.c"

struct val_list * scat_eval__(scat *this)
#line 148 "eval.tc"
{
	struct val_list *e1 = eval(this->expr1);
	struct val_list *e2 = eval(this->expr2);
	struct val_list *ret = NULL;
	struct val_list *ce1 = e1;
	struct val_list *ce2;
	
	while (ce1) {
		ce2 = e2;
		while (ce2) {
			if (ce1->count != 1) {
				yyerror("Argument 1 to scalar concatenation is not scalar");
				list_free(e1);
				list_free(e2);
				return error_val();
			}
			if (ce2->count != 1) {
				yyerror("Argument 2 to scalar concatenation is not scalar");
				list_free(e1);
				list_free(e2);
				return error_val();
			}
			struct val_list *cret = list_new(1, ce1->prob * ce2->prob);
			int i = 10;
			while (i < ce2->values[0]) {
				i *= 10;
			}
			cret->values[0] = ce1->values[0];
			cret->values[0] *= i;
			cret->values[0] += ce2->values[0];
			list_add(&ret, cret, this->ordering);
			ce2 = ce2->next;
		}
		ce1 = ce1->next;
	}
	list_free(e1);
	list_free(e2);
	return ret;
}
#line 1297 "tree.c"

struct scat_vtable__ const scat_vt__ = {
	&binary_vt__,
	scat_kind,
	"scat",
	(void (*)(expression *this__, int depth))scat_printtree__,
	(struct roll_value * (*)(expression *this__))scat_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))scat_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

void rep_printtree__(rep *this, int depth)
#line 76 "printtree.tc"
{
	indent(depth);
	printf("#\n");
	printtree(this->expr1, depth+1);
	printtree(this->expr2, depth+1);
}
#line 1319 "tree.c"

struct roll_value * rep_roll__(rep *this)
#line 132 "roll.tc"
{
	struct roll_value *rep = roll(this->expr1);
	if (rep->count != 1) {
		yyerror("Argument 1 to rep (#) isn't scalar");
		rep->count = 1;
		rep->values[0] = 0;
		return rep;
	}
	struct roll_value *rv = new_roll_single(rep->values[0]);
	rv->count = 0;
	int i;
	for (i = 0; i < rep->values[0]; i++) {
		struct roll_value *roll = roll(this->expr2);
		rv->values = (int*)realloc(rv->values, 
			sizeof(int) * (rv->count + roll->count));
		memcpy(&rv->values[rv->count], roll->values, 
			roll->count * sizeof(int)); 		
		rv->count += roll->count;
		free_roll(roll);
	}
	free_roll(rep);

	return rv;
}
#line 1347 "tree.c"

struct val_list * rep_eval__(rep *this)
#line 190 "eval.tc"
{
	struct val_list *cnt = eval(this->expr1);
	struct val_list *exp = eval(this->expr2);
	struct val_list *ret = NULL;
	struct val_list *ccnt = cnt;
	int i;

	while (ccnt) {
		if (ccnt->count != 1) {
			yyerror("Argument to repetition operator is not scalar");
			list_free(cnt);
			list_free(exp);
			return error_val();
		}
		// init everything
		int icnt = ccnt->values[0];
		if (icnt > 0) {
			int expcnt = 0;
			struct val_list *cexp = exp;
			while (cexp) {
				expcnt++;
				cexp = cexp->next;
			}
			int pos[icnt];
			memset(pos, 0, icnt * sizeof(int));
			struct val_list *index[expcnt];
			i = 0;
			cexp = exp;
			while (cexp) {
				index[i] = cexp;
				i++;
				cexp = cexp->next;
			}
			// the main iteration
			do {
				// do this set
				int len = 0;
				double prob = ccnt->prob;
				for (i = 0; i < icnt; i++) {
					len += index[pos[i]]->count;
					prob *= index[pos[i]]->prob;
				}
				struct val_list *cret = list_new(len, prob);
				int cp = 0;
				for (i = 0; i < icnt; i++) {
					memcpy(&cret->values[cp], index[pos[i]]->values, 
						index[pos[i]]->count * sizeof(int));
					cp += index[pos[i]]->count;
				}
				list_add(&ret, cret, this->ordering);

				// next iteration
				pos[0]++;
				i = 0;
				while ((pos[i] == expcnt) && (i < icnt-1)){
					pos[i] = 0;
					i++;
					pos[i]++;
				}
			} while (pos[icnt-1] < expcnt);
		} else {
			list_add(&ret, list_new(0, ccnt->prob), this->ordering);
		}
		// next iteration
		ccnt=ccnt->next;
	}
	// if we have nothing, return an empty list	
	if (ret == NULL) {
		struct val_list *cret = list_new(0, 1.0);
		list_add(&ret, cret, this->ordering);
	}
	list_free(cnt);
	list_free(exp);
	return ret;
}
#line 1426 "tree.c"

struct rep_vtable__ const rep_vt__ = {
	&binary_vt__,
	rep_kind,
	"rep",
	(void (*)(expression *this__, int depth))rep_printtree__,
	(struct roll_value * (*)(expression *this__))rep_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))rep_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

void range_printtree__(range *this, int depth)
#line 84 "printtree.tc"
{
	indent(depth);
	printf("..\n");
	printtree(this->expr1, depth+1);
	printtree(this->expr2, depth+1);
}
#line 1448 "tree.c"

struct roll_value * range_roll__(range *this)
#line 92 "roll.tc"
{
	int i;
	int c = 0;
	struct roll_value *rv1 = roll(this->expr1);
	struct roll_value *rv2 = roll(this->expr2);
	if (rv1->count != 1) {
		yyerror("Argument 1 to range (..) isn't scalar");
		free_roll(rv2);
		rv1->count = 1;
		rv1->values[0] = 0;
		return rv1;
	}
	if (rv2->count != 1) {
		yyerror("Argument 2 to range (..) isn't scalar");
		free_roll(rv2);
		rv1->count = 1;
		rv1->values[0] = 0;
		return rv1;
	}
	struct roll_value *rv;
	if (rv1->values[0] <= rv2->values[0]) {
		rv = new_roll_multi(
			abs(rv1->values[0] - rv2->values[0]) + 1);
		for (i = rv1->values[0]; i != rv2->values[0]; 
			rv1->values[0] < rv2->values[0] ? i++ : i--) {
			rv->values[c] = i;
			c++;
		}
		rv->values[c] = rv2->values[0];
	}
	else {
		rv = new_roll_single(0);
		rv->count = 0;
	}
	free_roll(rv1);
	free_roll(rv2);
	return rv;
}
#line 1490 "tree.c"

struct val_list * range_eval__(range *this)
#line 267 "eval.tc"
{
	struct val_list *from = eval(this->expr1);
	struct val_list *to = eval(this->expr2);
	struct val_list *ret = NULL;
	struct val_list *cfrom = from;
	struct val_list *cto;

	while (cfrom) {
		cto = to;
		while (cto) {
			// check both are scalar
			if (cfrom->count != 1) {
				yyerror("Argument \"from\" to range operator is not scalar");
				list_free(from);
				list_free(to);
				return error_val();
			}
			if (cto->count != 1) {
				yyerror("Argument \"to\" to range operator is not scalar");
				list_free(from);
				list_free(to);
				return error_val();
			}
			int ifrom = cfrom->values[0];
			int ito = cto->values[0];
			int count = ito - ifrom + 1;
			if (count < 0) {
				count = 0;
			}
			struct val_list *cret = list_new(count, cfrom->prob * cto->prob);
			int i, c;
			for (i = ifrom, c = 0; i <= ito; ++i, ++c) {
				cret->values[c] = i;
			}
			list_add(&ret, cret, this->ordering);

			cto = cto->next;
		}
		cfrom = cfrom->next;
	}
	list_free(from);
	list_free(to);
	return ret;
}
#line 1538 "tree.c"

struct range_vtable__ const range_vt__ = {
	&binary_vt__,
	range_kind,
	"range",
	(void (*)(expression *this__, int depth))range_printtree__,
	(struct roll_value * (*)(expression *this__))range_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))range_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

void lcat_printtree__(lcat *this, int depth)
#line 92 "printtree.tc"
{
	indent(depth);
	printf(",\n");
	printtree(this->expr1, depth+1);
	printtree(this->expr2, depth+1);
}
#line 1560 "tree.c"

struct roll_value * lcat_roll__(lcat *this)
#line 158 "roll.tc"
{
	struct roll_value *rv1 = roll(this->expr1);
	struct roll_value *rv2 = roll(this->expr2);
	rv1->values = (int*)realloc(rv1->values, 
		sizeof(int) * (rv1->count + rv2->count));
	memcpy(&rv1->values[rv1->count], rv2->values, 
		rv2->count * sizeof(int)); 		
	rv1->count += rv2->count;
	free_roll(rv2);
	return rv1;
}
#line 1575 "tree.c"

struct val_list * lcat_eval__(lcat *this)
#line 313 "eval.tc"
{
	struct val_list *e1 = eval(this->expr1);
	struct val_list *e2 = eval(this->expr2);
	struct val_list *ret = NULL;
	struct val_list *ce1 = e1;
	struct val_list *ce2;
	
	while (ce1) {
		ce2 = e2;
		while (ce2) {
			struct val_list *cret = list_new
				(ce1->count + ce2->count, ce1->prob * ce2->prob);
			memcpy(cret->values, ce1->values, ce1->count * sizeof(int));
			memcpy(&cret->values[ce1->count], ce2->values, 
				ce2->count * sizeof(int));
			list_add(&ret, cret, this->ordering);
			ce2 = ce2->next;
		}
		ce1 = ce1->next;
	}
	list_free(e1);
	list_free(e2);
	return ret;
}
#line 1603 "tree.c"

struct lcat_vtable__ const lcat_vt__ = {
	&binary_vt__,
	lcat_kind,
	"lcat",
	(void (*)(expression *this__, int depth))lcat_printtree__,
	(struct roll_value * (*)(expression *this__))lcat_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))lcat_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

struct filter_vtable__ const filter_vt__ = {
	&binary_vt__,
	filter_kind,
	"filter",
	(void (*)(expression *this__, int depth))0,
	(struct roll_value * (*)(expression *this__))0,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))0,
	(expression * (*)(expression *this__))binary_optimize__,
};

void let_printtree__(let *this, int depth)
#line 253 "printtree.tc"
{
	indent(depth);
	printf("let %s = \n", this->varname);
	printtree(this->expr1, depth+1);
	printtree(this->expr2, depth+1);
}
#line 1637 "tree.c"

struct roll_value * let_roll__(let *this)
#line 499 "roll.tc"
{
	// variable setzen und in symtab
	struct symtab *nst = (struct symtab*)malloc(sizeof(struct symtab));
	nst->name = this->varname;
	nst->rvalue = roll(this->expr1);
	nst->next = this->symtab;
	set_symtab(this->expr2, nst);
	struct roll_value *ret = roll(this->expr2);
	// variable wieder vom stack
	free_roll(nst->rvalue);
	free(nst);
	return ret;
}
#line 1654 "tree.c"

void let_set_ordering__(let *this, ordering_type ordering)
#line 94 "ordering.tc"
{
	this->ordering = ordering;
	struct symtab *nst = (struct symtab*)malloc(sizeof(struct symtab));
	nst->name = this->varname;
	nst->ordering = agnostic;
	nst->next = this->symtab;
	set_symtab(this->expr2, nst);
	set_ordering(this->expr2, ordering);
	set_ordering(this->expr1, nst->ordering);
	free(nst);
}
#line 1669 "tree.c"

struct val_list * let_eval__(let *this)
#line 672 "eval.tc"
{
	struct val_list *cnt = eval(this->expr1);
	struct val_list *ret = NULL;
	struct val_list *ccnt = cnt;
	
	while (ccnt) {
		struct symtab *nst = (struct symtab*)malloc(sizeof(struct symtab));
		nst->name = this->varname;
		nst->rvalue = (struct roll_value*)malloc(sizeof(struct roll_value));
		nst->rvalue->count = ccnt->count;
		nst->rvalue->values = (int*)malloc(sizeof(int) *  ccnt->count);
		memcpy(nst->rvalue->values, ccnt->values, sizeof(int) * ccnt->count);
		nst->next = this->symtab;
		set_symtab(this->expr2, nst);

		struct val_list *cur = eval(this->expr2);
		struct val_list *ccur = cur;
		while (ccur) {
			struct val_list *temp = ccur;
			ccur = ccur->next;
			temp->next = NULL;
			temp->prob *= ccnt->prob;
			list_add(&ret, temp, this->ordering);
		}

		free_roll(nst->rvalue);
		free(nst);
		ccnt = ccnt->next;
	}
	list_free(cnt);
	return ret;
}
#line 1705 "tree.c"

struct let_vtable__ const let_vt__ = {
	&binary_vt__,
	let_kind,
	"let",
	(void (*)(expression *this__, int depth))let_printtree__,
	(struct roll_value * (*)(expression *this__))let_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))let_set_ordering__,
	(struct val_list * (*)(expression *this__))let_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

void foreach_printtree__(foreach *this, int depth)
#line 261 "printtree.tc"
{
	indent(depth);
	printf("foreach %s \n", this->varname);
	printtree(this->expr1, depth+1);
	printtree(this->expr2, depth+1);
}
#line 1727 "tree.c"

struct roll_value * foreach_roll__(foreach *this)
#line 543 "roll.tc"
{
	struct roll_value *list = roll(this->expr1);
	struct roll_value *ret = (struct roll_value*)
		malloc(sizeof(struct roll_value));
	ret->count = 0;
	ret->values = NULL;
	struct symtab *nst = (struct symtab*)malloc(sizeof(struct symtab));
	nst->name = this->varname;
	nst->rvalue = (struct roll_value*)malloc(sizeof(struct roll_value));
	nst->rvalue->count = 1;
	nst->rvalue->values = (int*)malloc(sizeof(int));
	nst->next = this->symtab;
	set_symtab(this->expr2, nst);
	int i;
	for (i = 0; i < list->count; i++) {
		nst->rvalue->values[0] = list->values[i];
		struct roll_value *cur = roll(this->expr2);
		/* concat to the current solution */
		ret->values = (int*)realloc(ret->values, 
			(ret->count + cur->count) * sizeof(int));
		memcpy(&ret->values[ret->count], cur->values, 
			cur->count * sizeof(int));
		ret->count += cur->count;
		free_roll(cur);
	}
	free_roll(list);
	free_roll(nst->rvalue);
	free(nst);
	return ret;
}
#line 1761 "tree.c"

struct val_list * foreach_eval__(foreach *this)
#line 706 "eval.tc"
{
	struct val_list *cnt = eval(this->expr1);
	struct val_list *ccnt = cnt;
	int i;
	
	struct val_list *tret = NULL;

	struct symtab *nst = (struct symtab*)malloc(sizeof(struct symtab));
	nst->name = this->varname;
	nst->rvalue = (struct roll_value*)malloc(sizeof(struct roll_value));
	nst->rvalue->count = 1;
	nst->rvalue->values = (int*)malloc(sizeof(int));
	nst->next = this->symtab;
	set_symtab(this->expr2, nst);

	// each possibility
	while (ccnt) {
		struct val_list *ret = list_new(0, 1.0);
		for (i = 0; i < ccnt->count; i++) {
			nst->rvalue->values[0] = ccnt->values[i];
			struct val_list *cur = eval(this->expr2);
			// append each possibility in cur to each in ret, multiply 
			// the probability with the one from ccnt.
			struct val_list *cret = ret;
			struct val_list *nret = NULL;

			while (cret) {
				struct val_list *ccur = cur;
				while (ccur) {
 					struct val_list *item = list_new(
 						ccur->count + cret->count,
 						ccur->prob * cret->prob);
					memcpy(item->values, cret->values, 
						sizeof(int) * cret->count);
					memcpy(&item->values[cret->count], ccur->values, 
						sizeof(int) * ccur->count);
					
					list_add(&nret, item, this->ordering);
					
					ccur = ccur->next;
				}
				cret = cret->next;
			}
			list_free(cur);
			list_free(ret);
			ret = nret;
			nret = NULL;
		}
		struct val_list *cret = ret;
		while (cret) {
            cret->prob *= ccnt->prob;
			struct val_list *temp = cret;
			cret = cret->next;
			temp->next = NULL;
			list_add(&tret, temp, this->ordering);
		}
		ccnt = ccnt->next;
	}
	free_roll(nst->rvalue);
	free(nst);
	list_free(cnt);
	return tret;
}
#line 1828 "tree.c"

struct foreach_vtable__ const foreach_vt__ = {
	&binary_vt__,
	foreach_kind,
	"foreach",
	(void (*)(expression *this__, int depth))foreach_printtree__,
	(struct roll_value * (*)(expression *this__))foreach_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))foreach_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

void whiledo_printtree__(whiledo *this, int depth)
#line 269 "printtree.tc"
{
	indent(depth);
	printf("while %s \n", this->varname);
	printtree(this->expr1, depth+1);
	printtree(this->expr2, depth+1);
}
#line 1850 "tree.c"

struct roll_value * whiledo_roll__(whiledo *this)
#line 575 "roll.tc"
{
	struct roll_value *cur;
	struct roll_value *ret = (struct roll_value*)malloc(
		sizeof(struct roll_value));
	ret->count = 0;
	ret->values = NULL;
	struct symtab *nst = (struct symtab*)malloc(sizeof(struct symtab));
	nst->name = this->varname;
	nst->next = this->symtab;
	
	set_symtab(this->expr2, nst);
	cur = roll(this->expr1);
	nst->rvalue = cur;
	/* concat cur to ret */
	ret->values = (int*)realloc(ret->values, 
		sizeof(int) * (cur->count + ret->count));
	memcpy(&ret->values[ret->count], cur->values, sizeof(int) * cur->count);
	ret->count += cur->count;
	while (cur->count > 0)
	{
		cur = roll(this->expr2);
		free_roll(nst->rvalue);
		nst->rvalue = cur;
		/* concat cur to ret */
		ret->values = (int*)realloc(ret->values, 
			sizeof(int) * (cur->count + ret->count));
		memcpy(&ret->values[ret->count], cur->values, 
			sizeof(int) * cur->count);
		ret->count += cur->count;
	}
	free_roll(cur);
	free(nst);
	return ret;
}
#line 1888 "tree.c"

struct val_list * whiledo_eval__(whiledo *this)
#line 822 "eval.tc"
{
	struct val_list *expe = eval(this->expr1);
	struct val_list *ret = NULL;
	rec_whiledo(expe, this->varname, this->symtab, this->expr2, NULL, 0, &ret,
		this->ordering);
	list_free(expe);
	return ret;
}
#line 1900 "tree.c"

struct whiledo_vtable__ const whiledo_vt__ = {
	&binary_vt__,
	whiledo_kind,
	"whiledo",
	(void (*)(expression *this__, int depth))whiledo_printtree__,
	(struct roll_value * (*)(expression *this__))whiledo_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))whiledo_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

struct plus_vtable__ const plus_vt__ = {
	&mathop_vt__,
	plus_kind,
	"plus",
	(void (*)(expression *this__, int depth))mathop_printtree__,
	(struct roll_value * (*)(expression *this__))mathop_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))mathop_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

struct minus_vtable__ const minus_vt__ = {
	&mathop_vt__,
	minus_kind,
	"minus",
	(void (*)(expression *this__, int depth))mathop_printtree__,
	(struct roll_value * (*)(expression *this__))mathop_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))mathop_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

struct multi_vtable__ const multi_vt__ = {
	&mathop_vt__,
	multi_kind,
	"multi",
	(void (*)(expression *this__, int depth))mathop_printtree__,
	(struct roll_value * (*)(expression *this__))mathop_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))mathop_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

struct divi_vtable__ const divi_vt__ = {
	&mathop_vt__,
	divi_kind,
	"divi",
	(void (*)(expression *this__, int depth))mathop_printtree__,
	(struct roll_value * (*)(expression *this__))mathop_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))mathop_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

struct mod_vtable__ const mod_vt__ = {
	&mathop_vt__,
	mod_kind,
	"mod",
	(void (*)(expression *this__, int depth))mathop_printtree__,
	(struct roll_value * (*)(expression *this__))mathop_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))mathop_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

struct expo_vtable__ const expo_vt__ = {
	&mathop_vt__,
	expo_kind,
	"expo",
	(void (*)(expression *this__, int depth))mathop_printtree__,
	(struct roll_value * (*)(expression *this__))mathop_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))mathop_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

void first_printtree__(first *this, int depth)
#line 149 "printtree.tc"
{
	indent(depth);
	if (this->type == drop) {
		printf("drop ");
	}
	else {
		printf("keep ");
	}
	printf("first\n");
	printtree(this->expr1, depth+1);
	printtree(this->expr2, depth+1);
}
#line 2000 "tree.c"

struct roll_value * first_roll__(first *this)
#line 266 "roll.tc"
{
	struct roll_value *num = roll(this->expr1);
	struct roll_value *exp = roll(this->expr2);
	if (num->count != 1) {
		yyerror("Argument 1 to first isn't scalar");
		free_roll(num);
		exp->count = 1;
		exp->values[0] = 0;
		return exp;
	}
	if (this->type == drop) {
		int i = exp->count - num->values[0];
		if (i <= 0) {
			i = 0;
			exp->count = 0;
		}
		else {
			memcpy(&exp->values[0], &exp->values[exp->count - i], 
				i * sizeof(int));
			exp->count = i;
		}
	}
	else {
		exp->count = min(exp->count, num->values[0]);
	}
	free_roll(num);
	return exp;
}
#line 2032 "tree.c"

void first_set_ordering__(first *this, ordering_type ordering)
#line 55 "ordering.tc"
{
	this->ordering = ordering;
	set_ordering(this->expr1, agnostic);
	set_ordering(this->expr2, caring);
}
#line 2041 "tree.c"

struct val_list * first_eval__(first *this)
#line 339 "eval.tc"
{
	struct val_list *num = eval(this->expr1);
	struct val_list *exp = eval(this->expr2);
	struct val_list *ret = NULL;
	struct val_list *cnum = num;
	struct val_list *cexp;
	
	while (cnum) {
		// make sure cnum is skalar
		if (cnum->count != 1) {
			yyerror("Argument 1 to \"first\" operator is not scalar");
			list_free(exp);
			list_free(num);
			return error_val();
		}
		cexp = exp;
		while (cexp) {
			struct val_list *cret = (struct val_list*)
				malloc(sizeof(struct val_list));
			cret->next = NULL;
			if (this->type == drop) {
				cret->count = cexp->count - cnum->values[0];
				if (cret->count <= 0) {
					cret->count = 0;
				}
				cret->values = (int*)malloc(cret->count * sizeof(int));
				memcpy(cret->values, &cexp->values[cexp->count - cret->count], 
						cret->count * sizeof(int));
			}
			else {
				cret->count = min(cexp->count, cnum->values[0]);
				cret->values = (int*)malloc(cret->count * sizeof(int));
				memcpy(cret->values, cexp->values, cret->count * sizeof(int));
			}
			cret->prob = cexp->prob * cnum->prob;
			list_add(&ret, cret, this->ordering);
			cexp = cexp->next;
		}
		cnum = cnum->next;
	}
	list_free(exp);
	list_free(num);
	return ret;
}
#line 2089 "tree.c"

struct first_vtable__ const first_vt__ = {
	&filter_vt__,
	first_kind,
	"first",
	(void (*)(expression *this__, int depth))first_printtree__,
	(struct roll_value * (*)(expression *this__))first_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))first_set_ordering__,
	(struct val_list * (*)(expression *this__))first_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

void last_printtree__(last *this, int depth)
#line 163 "printtree.tc"
{
	indent(depth);
	if (this->type == drop) {
		printf("drop ");
	}
	else {
		printf("keep ");
	}
	printf("last\n");
	printtree(this->expr1, depth+1);
	printtree(this->expr2, depth+1);
}
#line 2117 "tree.c"

struct roll_value * last_roll__(last *this)
#line 295 "roll.tc"
{
	struct roll_value *num = roll(this->expr1);
	struct roll_value *exp = roll(this->expr2);
	if (num->count != 1) {
		yyerror("Argument 1 to last isn't scalar");
		free_roll(num);
		exp->count = 1;
		exp->values[0] = 0;
		return exp;
	}
	if (this->type == drop) {
		exp->count -= num->values[0];
		if (exp->count < 0) {
			exp->count = 0;
		}
	}
	else {
		if (num->values[0] < exp->count) {
			memcpy(&exp->values[0], &exp->values[exp->count - num->values[0]],
				num->values[0] * sizeof(int));
			exp->count = num->values[0];
		}
	}
	free_roll(num);
	return exp;
}
#line 2147 "tree.c"

void last_set_ordering__(last *this, ordering_type ordering)
#line 62 "ordering.tc"
{
	this->ordering = ordering;
	set_ordering(this->expr1, agnostic);
	set_ordering(this->expr2, caring);
}
#line 2156 "tree.c"

struct val_list * last_eval__(last *this)
#line 385 "eval.tc"
{
	struct val_list *num = eval(this->expr1);
	struct val_list *exp = eval(this->expr2);
	struct val_list *ret = NULL;
	struct val_list *cnum = num;
	struct val_list *cexp;
	
	while (cnum) {
		// make sure cnum is skalar
		if (cnum->count != 1) {
			yyerror("Argument 1 to \"last\" operator is not scalar");
			list_free(exp);
			list_free(num);
			return error_val();
		}
		cexp = exp;
		while (cexp) {
			struct val_list *cret = (struct val_list*)
				malloc(sizeof(struct val_list));
			cret->next = NULL;
			if (this->type == drop) {
				cret->count = max(cexp->count - cnum->values[0], 0);
				cret->values = (int*)malloc(cret->count * sizeof(int));
				memcpy(cret->values, cexp->values, cret->count * sizeof(int));
			}
			else {
				cret->count = min(cexp->count, cnum->values[0]);
				cret->values = (int*)malloc(cret->count * sizeof(int));
				memcpy(cret->values, &cexp->values[cexp->count - cret->count], 
					cret->count * sizeof(int));
				// XXX this block is complete bollocks
/*				if (cnum->values[0] < cexp->count) {
					cret->count = cnum->values[0];
					cret->values = (int*)malloc(cret->count * sizeof(int));
					memcpy(cret->values, &cexp->values[cexp->count -
					cnum->values[0]], cret->count * sizeof(int));
				}*/
			}
			cret->prob = cexp->prob * cnum->prob;
			list_add(&ret, cret, this->ordering);
			cexp = cexp->next;
		}
		cnum = cnum->next;
	}
	list_free(exp);
	list_free(num);
	return ret;
}
#line 2208 "tree.c"

struct last_vtable__ const last_vt__ = {
	&filter_vt__,
	last_kind,
	"last",
	(void (*)(expression *this__, int depth))last_printtree__,
	(struct roll_value * (*)(expression *this__))last_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))last_set_ordering__,
	(struct val_list * (*)(expression *this__))last_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

void high_printtree__(high *this, int depth)
#line 177 "printtree.tc"
{
	indent(depth);
	if (this->type == drop) {
		printf("drop ");
	}
	else {
		printf("keep ");
	}
	printf("high\n");
	printtree(this->expr1, depth+1);
	printtree(this->expr2, depth+1);
}
#line 2236 "tree.c"

struct roll_value * high_roll__(high *this)
#line 381 "roll.tc"
{
	struct roll_value *num = roll(this->expr1);
	struct roll_value *exp = roll(this->expr2);
	if (num->count != 1) {
		yyerror("Argument 1 to last isn't scalar");
		free_roll(num);
		exp->count = 1;
		exp->values[0] = 0;
		return exp;
	}
	int *slist = (int*)malloc(sizeof(int) * exp->count);
	int *mask = (int*)malloc(sizeof(int) * exp->count);
	memcpy(slist, exp->values, sizeof(int) * exp->count);
	memset(mask, 0, sizeof(int) * exp->count);
	quicksort(slist, 0, exp->count -1);
	int p = 0, i, c = 0;

	if (this->type == keep) {
		for (i = 0; (i < exp->count) && (c < num->values[0]); i++) {
			int j, found = 0;
			for (j = exp->count - 1; j >= exp->count - num->values[0]; j--) {
				if ((exp->values[i] == slist[j]) && (!mask[j])) {
					found = 1;
                    mask[j] = 1;
					break;
				}
			}
			if (found) {
				exp->values[p++] = exp->values[i];
				c++;
			}
		}
	}
	else {
		for (i = 0; (i < exp->count) && (c < (exp->count - num->values[0])); 
			i++) {
			int j, found = 0;
			for (j = 0; j < exp->count - num->values[0]; j++) {
				if ((exp->values[i] == slist[j]) && (!mask[j])) {
					found = 1;
					mask[j] = 1;
					break;
				}
			}
			if (found) {
				exp->values[p++] = exp->values[i];
				c++;
			}
		}
	}
	exp->count = c;
	free_roll(num);
	free(slist);
	free(mask);
	return exp;
}
#line 2296 "tree.c"

struct val_list * high_eval__(high *this)
#line 435 "eval.tc"
{
	struct val_list *num = eval(this->expr1);
	struct val_list *exp = eval(this->expr2);
	struct val_list *ret = NULL;
	struct val_list *cnum = num;
	struct val_list *cexp;
	
	while (cnum) {
		// make sure cnum is skalar
		if (cnum->count != 1) {
			yyerror("Argument 1 to \"high\" operator is not scalar");
			list_free(exp);
			list_free(num);
			return error_val();
		}
		cexp = exp;
		while (cexp) {
			struct val_list *cret = (struct val_list*)
				malloc(sizeof(struct val_list));
			cret->next = NULL;

            // ready, start the actual filtering:
            // we want up to cnum->values[0] items from cexp in cret

			int *slist = (int*)malloc(sizeof(int) * cexp->count);
			int *mask = (int*)malloc(sizeof(int) * cexp->count);
			memcpy(slist, cexp->values, sizeof(int) * cexp->count);
			memset(mask, 0, sizeof(int) * cexp->count);
			quicksort(slist, 0, cexp->count - 1);
            reverse(slist, 0, cexp->count - 1);
		
        	int p = 0, i;
			if (this->type == keep) {
                // how many do we want?
				cret->count = min(cexp->count, cnum->values[0]);
				cret->values = (int*)malloc(sizeof(int) * cret->count);

				for (i = 0; (i < cexp->count) && (p < cnum->values[0]); i++) {
					int j, found = 0;
					for (j = 0; j < cnum->values[0]; j++) {
						if ((cexp->values[i] == slist[j]) && (!mask[j])) {
							found = 1;
                            mask[j] = 1;
							break;
						}
					}
					if (found) {
						cret->values[p++] = cexp->values[i];
					}
				}
			}
			else {
				cret->count = max(cexp->count - cnum->values[0], 0);
				cret->values = (int*)malloc(sizeof(int) * cret->count);
				for (i = 0; (i < cexp->count) 
					&& (p < (cexp->count - cnum->values[0])); 
					i++) {
					int j, found = 0;
					for (j = cexp->count - 1; j >= cnum->values[0]; j--) {
						if ((cexp->values[i] == slist[j]) && (!mask[j])) {
							found = 1;
							// XXX der mask kram auch bei high, und auch im
							// roll.tc
							mask[j] = 1;
							break;
						}
					}
					if (found) {
						cret->values[p++] = cexp->values[i];
					}
				}
			}
			free(slist);
			free(mask);
			cret->prob = cexp->prob * cnum->prob;	
			list_add(&ret, cret, this->ordering);
			cexp = cexp->next;
		}
		cnum = cnum->next;
	}
	list_free(exp);
	list_free(num);
	return ret;
}
#line 2384 "tree.c"

struct high_vtable__ const high_vt__ = {
	&filter_vt__,
	high_kind,
	"high",
	(void (*)(expression *this__, int depth))high_printtree__,
	(struct roll_value * (*)(expression *this__))high_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))high_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

void low_printtree__(low *this, int depth)
#line 191 "printtree.tc"
{
	indent(depth);
	if (this->type == drop) {
		printf("drop ");
	}
	else {
		printf("keep ");
	}
	printf("low\n");
	printtree(this->expr1, depth+1);
	printtree(this->expr2, depth+1);
}
#line 2412 "tree.c"

struct roll_value * low_roll__(low *this)
#line 322 "roll.tc"
{
	struct roll_value *num = roll(this->expr1);
	struct roll_value *exp = roll(this->expr2);
	if (num->count != 1) {
		yyerror("Argument 1 to last isn't scalar");
		free_roll(num);
		exp->count = 1;
		exp->values[0] = 0;
		return exp;
	}
	int *slist = (int*)malloc(sizeof(int) * exp->count);
	int *mask = (int*)malloc(sizeof(int) * exp->count);
	memcpy(slist, exp->values, sizeof(int) * exp->count);
	memset(mask, 0, sizeof(int) * exp->count);
	quicksort(slist, 0, exp->count -1);
	int p = 0, i, c = 0;

	if (this->type == keep) {
		for (i = 0; (i < exp->count) && (c < num->values[0]); i++) {
			int j, found = 0;
			for (j = 0; j < num->values[0]; j++) {
				if ((exp->values[i] == slist[j]) && (!mask[j])) {
					found = 1;
                    mask[j] = 1;
					break;
				}
			}
			if (found) {
				exp->values[p++] = exp->values[i];
				c++;
			}
		}
	}
	else {
		for (i = 0; (i < exp->count) && (c < (exp->count - num->values[0])); 
			i++) {
			int j, found = 0;
			for (j = exp->count - 1; j >= num->values[0]; j--) {
				if ((exp->values[i] == slist[j]) && (!mask[j])) {
					found = 1;
					mask[j] = 1;
					break;
				}
				else {
				}
			}
			if (found) {
				exp->values[p++] = exp->values[i];
				c++;
			}
		}
	}
	exp->count = c;
	free_roll(num);
	free(slist);
	free(mask);
	return exp;
}
#line 2474 "tree.c"

struct val_list * low_eval__(low *this)
#line 521 "eval.tc"
{
	struct val_list *num = eval(this->expr1);
	struct val_list *exp = eval(this->expr2);
	struct val_list *ret = NULL;
	struct val_list *cnum = num;
	struct val_list *cexp;
	
	while (cnum) {
		// make sure cnum is skalar
		if (cnum->count != 1) {
			yyerror("Argument 1 to \"low\" operator is not scalar");
			list_free(exp);
			list_free(num);
			return error_val();
		}
		cexp = exp;
		while (cexp) {
			struct val_list *cret = (struct val_list*)
				malloc(sizeof(struct val_list));
			cret->next = NULL;

            // ready, start the actual filtering:
            // we want up to cnum->values[0] items from cexp in cret

			int *slist = (int*)malloc(sizeof(int) * cexp->count);
			int *mask = (int*)malloc(sizeof(int) * cexp->count);
			memcpy(slist, cexp->values, sizeof(int) * cexp->count);
			memset(mask, 0, sizeof(int) * cexp->count);
			quicksort(slist, 0, cexp->count - 1);
		
        	int p = 0, i;
			if (this->type == keep) {
                // how many do we want?
				cret->count = min(cexp->count, cnum->values[0]);
				cret->values = (int*)malloc(sizeof(int) * cret->count);

				for (i = 0; (i < cexp->count) && (p < cnum->values[0]); i++) {
					int j, found = 0;
					for (j = 0; j < cnum->values[0]; j++) {
						if ((cexp->values[i] == slist[j]) && (!mask[j])) {
							found = 1;
                            mask[j] = 1;
							break;
						}
					}
					if (found) {
						cret->values[p++] = cexp->values[i];
					}
				}
			}
			else {
				cret->count = max(cexp->count - cnum->values[0], 0);
				cret->values = (int*)malloc(sizeof(int) * cret->count);
				for (i = 0; (i < cexp->count) 
					&& (p < (cexp->count - cnum->values[0])); 
					i++) {
					int j, found = 0;
					for (j = cexp->count - 1; j >= cnum->values[0]; j--) {
						if ((cexp->values[i] == slist[j]) && (!mask[j])) {
							found = 1;
							// XXX der mask kram auch bei high, und auch im
							// roll.tc
							mask[j] = 1;
							break;
						}
					}
					if (found) {
						cret->values[p++] = cexp->values[i];
					}
				}
			}
			free(slist);
			free(mask);
			cret->prob = cexp->prob * cnum->prob;	
			list_add(&ret, cret, this->ordering);
			cexp = cexp->next;
		}
		cnum = cnum->next;
	}
	list_free(exp);
	list_free(num);
	return ret;
}
#line 2561 "tree.c"

struct low_vtable__ const low_vt__ = {
	&filter_vt__,
	low_kind,
	"low",
	(void (*)(expression *this__, int depth))low_printtree__,
	(struct roll_value * (*)(expression *this__))low_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))low_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

void comparison_printtree__(comparison *this, int depth)
#line 205 "printtree.tc"
{
	indent(depth);
	if (this->type == drop) {
		printf("drop ");
	}
	else {
		printf("keep ");
	}
	printf("comparison ");
	switch (this->comp) {
		case eq:
			printf("eq\n");
			break;
		case ne:
			printf("ne\n");
			break;
		case gt:
			printf("gt\n");
			break;
		case lt:
			printf("lt\n");
			break;
		case ge:
			printf("ge\n");
			break;
		case le:
			printf("le\n");
			break;
		default:
			yyerror("Unexpected comparison (really bad)");
	}
	printtree(this->expr1, depth+1);
	printtree(this->expr2, depth+1);
}
#line 2611 "tree.c"

struct roll_value * comparison_roll__(comparison *this)
#line 438 "roll.tc"
{
	struct roll_value *num = roll(this->expr1);
	struct roll_value *exp = roll(this->expr2);
	if (num->count != 1) {
		yyerror("Argument 1 to comparison isn't scalar");
		free_roll(num);
		exp->count = 1;
		exp->values[0] = 0;
		return exp;
	}
	int o, n = 0;
	for (o = 0; o < exp->count; o++) {
		int c = 0;
		switch (this->comp) {
			case eq:
				c = exp->values[o] == num->values[0];
				break;
			case ne:
				c = exp->values[o] != num->values[0];
				break;
			case gt:
				c = exp->values[o] > num->values[0];
				break;
			case lt:
				c = exp->values[o] < num->values[0];
				break;
			case ge:
				c = exp->values[o] >= num->values[0];
				break;
			case le:
				c = exp->values[o] <= num->values[0];
				break;
			default:
				yyerror("Unexpected comparison (really bad)");
		}
		if (this->type == drop) {
			c = !c;
		}
		if (c) {
			exp->values[n++] = exp->values[o];
		}
	}
	exp->count = n;
	free_roll(num);
	return exp;
}
#line 2661 "tree.c"

struct val_list * comparison_eval__(comparison *this)
#line 606 "eval.tc"
{
	struct val_list *num = eval(this->expr1);
	struct val_list *exp = eval(this->expr2);
	struct val_list *ret = NULL;
	struct val_list *cnum = num;
	struct val_list *cexp;
	
	while (cnum) {
		// make sure cnum is skalar
		if (cnum->count != 1) {
			yyerror("Argument 1 to comparison operator is not scalar");
			list_free(exp);
			list_free(num);
			return error_val();
		}
		cexp = exp;
		while (cexp) {
			struct val_list *cret = list_new
				(cexp->count, cexp->prob * cnum->prob);
			cret->count = 0; // updated below
			int o;
			for (o = 0; o < cexp->count; o++) {
				int c = 0;
				switch (this->comp) {
					case eq:
						c = cexp->values[o] == cnum->values[0];
						break;
					case ne:
						c = cexp->values[o] != cnum->values[0];
						break;
					case gt:
						c = cexp->values[o] > cnum->values[0];
						break;
					case lt:
						c = cexp->values[o] < cnum->values[0];
						break;
					case ge:
						c = cexp->values[o] >= cnum->values[0];
						break;
					case le:
						c = cexp->values[o] <= cnum->values[0];
						break;
					default:
						yyerror("Unexpected comparison (really bad)");
						list_free(exp);
						list_free(num);
						return error_val();
				}
				if (this->type == drop) {
					c = !c;
				}
				if (c) {
					cret->values[cret->count++] = cexp->values[o];
				}
			}
			list_add(&ret, cret, this->ordering);
			cexp = cexp->next;
		}
		cnum = cnum->next;
	}
	list_free(exp);
	list_free(num);
	return ret;
}
#line 2729 "tree.c"

struct comparison_vtable__ const comparison_vt__ = {
	&filter_vt__,
	comparison_kind,
	"comparison",
	(void (*)(expression *this__, int depth))comparison_printtree__,
	(struct roll_value * (*)(expression *this__))comparison_roll__,
	(void (*)(expression *this__, struct symtab * st))binary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))binary_set_ordering__,
	(struct val_list * (*)(expression *this__))comparison_eval__,
	(expression * (*)(expression *this__))binary_optimize__,
};

void negate_printtree__(negate *this, int depth)
#line 31 "printtree.tc"
{
	indent(depth);
	printf("-\n");
	printtree(this->expr, depth+1);
}
#line 2750 "tree.c"

struct roll_value * negate_roll__(negate *this)
#line 171 "roll.tc"
{
	struct roll_value *rv = roll(this->expr);
	if (rv->count != 1) {
		yyerror("Argument 1 to negate (-) isn't scalar");
		rv->count = 1;
		rv->values[0] = 0;
		return rv;
	}
	rv->values[0] *= -1;
	return rv;
}
#line 2765 "tree.c"

struct val_list * negate_eval__(negate *this)
#line 832 "eval.tc"
{
	struct val_list *sides = eval(this->expr);
	struct val_list *cside = sides;
	
	while (cside) {
		// make sure this is scalar
		if (cside->count != 1) {
			yyerror("Argument to negate operator is not scalar");
			return error_val();
		}
		cside->values[0] *= -1;
		cside = cside->next;
	}
	return sides;
}
#line 2784 "tree.c"

struct negate_vtable__ const negate_vt__ = {
	&unary_vt__,
	negate_kind,
	"negate",
	(void (*)(expression *this__, int depth))negate_printtree__,
	(struct roll_value * (*)(expression *this__))negate_roll__,
	(void (*)(expression *this__, struct symtab * st))unary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))unary_set_ordering__,
	(struct val_list * (*)(expression *this__))negate_eval__,
	(expression * (*)(expression *this__))unary_optimize__,
};

void dice_printtree__(dice *this, int depth)
#line 100 "printtree.tc"
{
	indent(depth);
	printf("d\n");
	printtree(this->expr, depth+1);
}
#line 2805 "tree.c"

struct roll_value * dice_roll__(dice *this)
#line 184 "roll.tc"
{
	struct roll_value *rv = roll(this->expr);
	if (rv->count != 1) {
		yyerror("Argument 1 to dice (d) isn't scalar");
		rv->count = 1;
		rv->values[0] = 0;
		return rv;
	}
	if (rv->values[0] < 1) {
		yyerror("Argument 1 to dice (d) isn't >= 1");
		rv->count = 1;
		rv->values[0] = 0;
		return rv;
	}
	rv->values[0] = 1 + (int) (rv->values[0] * (rand() / (RAND_MAX + 1.0)));
	return rv;
}
#line 2826 "tree.c"

struct val_list * dice_eval__(dice *this)
#line 849 "eval.tc"
{
	struct val_list *sides = eval(this->expr);
	struct val_list *ret = NULL;
	
	struct val_list *cside = sides;
	while (cside) {
		// make sure this is scalar and > 0
		if (cside->count != 1) {
			yyerror("Argument to \"dice\" operator is not scalar");
			list_free(sides);
			return error_val();
		}
		if (cside->values[0] <= 0) {
			yyerror("Argument to \"dice\" operator is not > 0");
			list_free(sides);
			return error_val();
		}
		int i;
		for (i = 1; i <= cside->values[0]; i++) {
			// create a matching result
			struct val_list *cret = list_new(1, cside->prob/cside->values[0]);
			cret->values[0] = i;
			list_add(&ret, cret, this->ordering);
		}
		cside = cside->next;
	}
	list_free(sides);
	return ret;
}
#line 2859 "tree.c"

struct dice_vtable__ const dice_vt__ = {
	&unary_vt__,
	dice_kind,
	"dice",
	(void (*)(expression *this__, int depth))dice_printtree__,
	(struct roll_value * (*)(expression *this__))dice_roll__,
	(void (*)(expression *this__, struct symtab * st))unary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))unary_set_ordering__,
	(struct val_list * (*)(expression *this__))dice_eval__,
	(expression * (*)(expression *this__))unary_optimize__,
};

void sum_printtree__(sum *this, int depth)
#line 107 "printtree.tc"
{
	indent(depth);
	printf("sum\n");
	printtree(this->expr, depth+1);
}
#line 2880 "tree.c"

struct roll_value * sum_roll__(sum *this)
#line 203 "roll.tc"
{
	int s = 0;
	int i;
	struct roll_value *rv = roll(this->expr);
	for (i = 0; i < rv->count; i++) {
		s += rv->values[i];
	}
	rv->count = 1;
	rv->values = (int*)realloc(rv->values, sizeof(int) * rv->count);
	rv->values[0] = s;
	return rv;
}
#line 2896 "tree.c"

void sum_set_ordering__(sum *this, ordering_type ordering)
#line 25 "ordering.tc"
{
	this->ordering = ordering;
	set_ordering(this->expr, agnostic);
}
#line 2904 "tree.c"

struct val_list * sum_eval__(sum *this)
#line 880 "eval.tc"
{
	struct val_list *sides = eval(this->expr);
	struct val_list *ret = NULL;
	struct val_list *cside = sides;
	
	while (cside) {
		int sum = 0;
		int i;
		for (i = 0; i < cside->count; i++) {
			sum += cside->values[i];
		}
		struct val_list *cret = list_new(1, cside->prob);
		cret->values[0] = sum;
		list_add(&ret, cret, this->ordering);
		cside = cside->next;
	}
	list_free(sides);
	return ret;
}
#line 2927 "tree.c"

expression * sum_optimize__(sum *this)
#line 251 "optimize.tc"
{
	if (yykind(this->expr) == rep_kind) {
		rep *crep = (rep*)this->expr;
		if (yykind(crep->expr2) == dice_kind) {
			dice *cdice = (dice*)crep->expr2;
			if (yykind(cdice->expr) == number_kind) {
// case: sum N#dM
				expression *replace = sumrepdice_create(
					eval(crep->expr1), 
					((number*)cdice->expr)->num);
				return replace;
			}
		}
		else {
// case: sum N#XXX
			expression *replace = sumrepany_create(
				eval(crep->expr1),
				eval(sum_create(crep->expr2))); 
			return replace;			
		}
	}
	return (expression*)this;
}
#line 2954 "tree.c"

struct sum_vtable__ const sum_vt__ = {
	&unary_vt__,
	sum_kind,
	"sum",
	(void (*)(expression *this__, int depth))sum_printtree__,
	(struct roll_value * (*)(expression *this__))sum_roll__,
	(void (*)(expression *this__, struct symtab * st))unary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))sum_set_ordering__,
	(struct val_list * (*)(expression *this__))sum_eval__,
	(expression * (*)(expression *this__))sum_optimize__,
};

void prod_printtree__(prod *this, int depth)
#line 114 "printtree.tc"
{
	indent(depth);
	printf("prod\n");
	printtree(this->expr, depth+1);
}
#line 2975 "tree.c"

struct roll_value * prod_roll__(prod *this)
#line 217 "roll.tc"
{
	int s = 1;
	int i;
	struct roll_value *rv = roll(this->expr);
	for (i = 0; i < rv->count; i++) {
		s *= rv->values[i];
	}
	rv->count = 1;
	rv->values = (int*)realloc(rv->values, sizeof(int) * rv->count);
	rv->values[0] = s;
	return rv;
}
#line 2991 "tree.c"

void prod_set_ordering__(prod *this, ordering_type ordering)
#line 31 "ordering.tc"
{
	this->ordering = ordering;
	set_ordering(this->expr, agnostic);
}
#line 2999 "tree.c"

struct val_list * prod_eval__(prod *this)
#line 901 "eval.tc"
{
	struct val_list *sides = eval(this->expr);
	struct val_list *ret = NULL;
	struct val_list *cside = sides;
	
	while (cside) {
		int prod = 1;
		int i;
		for (i = 0; i < cside->count; i++) {
			prod *= cside->values[i];
		}
		struct val_list *cret = list_new(1, cside->prob);
		cret->values[0] = prod;
		list_add(&ret, cret, this->ordering);
		cside = cside->next;
	}
	list_free(sides);
	return ret;
}
#line 3022 "tree.c"

struct prod_vtable__ const prod_vt__ = {
	&unary_vt__,
	prod_kind,
	"prod",
	(void (*)(expression *this__, int depth))prod_printtree__,
	(struct roll_value * (*)(expression *this__))prod_roll__,
	(void (*)(expression *this__, struct symtab * st))unary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))prod_set_ordering__,
	(struct val_list * (*)(expression *this__))prod_eval__,
	(expression * (*)(expression *this__))unary_optimize__,
};

void count_printtree__(count *this, int depth)
#line 121 "printtree.tc"
{
	indent(depth);
	printf("count\n");
	printtree(this->expr, depth+1);
}
#line 3043 "tree.c"

struct roll_value * count_roll__(count *this)
#line 258 "roll.tc"
{
	struct roll_value *rv = roll(this->expr);
	rv->values = realloc(rv->values, sizeof(int));
	rv->values[0] = rv->count;
	rv->count = 1;
	return rv;
}
#line 3054 "tree.c"

void count_set_ordering__(count *this, ordering_type ordering)
#line 37 "ordering.tc"
{
	this->ordering = ordering;
	set_ordering(this->expr, agnostic);
}
#line 3062 "tree.c"

struct val_list * count_eval__(count *this)
#line 922 "eval.tc"
{
	struct val_list *sides = eval(this->expr);
	struct val_list *ret = NULL;
	struct val_list *cside = sides;
	
	while (cside) {
		struct val_list *cret = list_new(1, cside->prob);
		cret->values[0] = cside->count;
		list_add(&ret, cret, this->ordering);
		cside = cside->next;
	}
	list_free(sides);
	return ret;
}
#line 3080 "tree.c"

struct count_vtable__ const count_vt__ = {
	&unary_vt__,
	count_kind,
	"count",
	(void (*)(expression *this__, int depth))count_printtree__,
	(struct roll_value * (*)(expression *this__))count_roll__,
	(void (*)(expression *this__, struct symtab * st))unary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))count_set_ordering__,
	(struct val_list * (*)(expression *this__))count_eval__,
	(expression * (*)(expression *this__))unary_optimize__,
};

void perm_printtree__(perm *this, int depth)
#line 128 "printtree.tc"
{
	indent(depth);
	printf("perm\n");
	printtree(this->expr, depth+1);
}
#line 3101 "tree.c"

struct roll_value * perm_roll__(perm *this)
#line 231 "roll.tc"
{
	struct roll_value *rv = roll(this->expr);
	permute(rv->values, rv->count);
	return rv;
}
#line 3110 "tree.c"

void perm_set_ordering__(perm *this, ordering_type ordering)
#line 49 "ordering.tc"
{
	this->ordering = ordering;
	set_ordering(this->expr, agnostic);
}
#line 3118 "tree.c"

struct val_list * perm_eval__(perm *this)
#line 950 "eval.tc"
{
	struct val_list *exp = eval(this->expr);
	struct val_list *cexp = exp;
	struct val_list *ret = NULL;

	if (this->ordering == agnostic) {
		return exp;
	}

	while(cexp) {
		all_permutations(cexp->values, cexp->count, cb_perm, &ret, 
			cexp->prob / factorial(cexp->count));	
		cexp = cexp->next;
	}
	list_free(exp);
	return ret;
}
#line 3139 "tree.c"

struct perm_vtable__ const perm_vt__ = {
	&unary_vt__,
	perm_kind,
	"perm",
	(void (*)(expression *this__, int depth))perm_printtree__,
	(struct roll_value * (*)(expression *this__))perm_roll__,
	(void (*)(expression *this__, struct symtab * st))unary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))perm_set_ordering__,
	(struct val_list * (*)(expression *this__))perm_eval__,
	(expression * (*)(expression *this__))unary_optimize__,
};

void sort_printtree__(sort *this, int depth)
#line 135 "printtree.tc"
{
	indent(depth);
	printf("sort\n");
	printtree(this->expr, depth+1);
}
#line 3160 "tree.c"

struct roll_value * sort_roll__(sort *this)
#line 238 "roll.tc"
{
	struct roll_value *rv = roll(this->expr);
	/* sortiere */
	quicksort(rv->values, 0, rv->count - 1);
	return rv;
}
#line 3170 "tree.c"

void sort_set_ordering__(sort *this, ordering_type ordering)
#line 43 "ordering.tc"
{
	this->ordering = ordering;
	set_ordering(this->expr, agnostic);
}
#line 3178 "tree.c"

struct val_list * sort_eval__(sort *this)
#line 969 "eval.tc"
{
	struct val_list *sides = eval(this->expr);
	struct val_list *cside = sides;
	
	if (this->ordering != agnostic) {
		while (cside) {
			quicksort(cside->values, 0, cside->count - 1); 
			cside = cside->next;
		}
	}
	return sides;
}
#line 3194 "tree.c"

struct sort_vtable__ const sort_vt__ = {
	&unary_vt__,
	sort_kind,
	"sort",
	(void (*)(expression *this__, int depth))sort_printtree__,
	(struct roll_value * (*)(expression *this__))sort_roll__,
	(void (*)(expression *this__, struct symtab * st))unary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))sort_set_ordering__,
	(struct val_list * (*)(expression *this__))sort_eval__,
	(expression * (*)(expression *this__))unary_optimize__,
};

void rev_printtree__(rev *this, int depth)
#line 142 "printtree.tc"
{
	indent(depth);
	printf("rev\n");
	printtree(this->expr, depth+1);
}
#line 3215 "tree.c"

struct roll_value * rev_roll__(rev *this)
#line 246 "roll.tc"
{
	int i;
	struct roll_value *rv = roll(this->expr);
	for (i = 0; i < rv->count/2; i++) {
		rv->values[i] ^= rv->values[rv->count-1-i];
		rv->values[rv->count-1-i] ^= rv->values[i];
		rv->values[i] ^= rv->values[rv->count-1-i];
	}
	return rv;
}
#line 3229 "tree.c"

struct val_list * rev_eval__(rev *this)
#line 983 "eval.tc"
{
	struct val_list *sides = eval(this->expr);
	struct val_list *cside = sides;
	
	if (this->ordering != agnostic) {
		while (cside) {
			int i, j;
			i = 0; j = cside->count-1;
			while (i < j) {
				int t = cside->values[i];
				cside->values[i] = cside->values[j];
				cside->values[j] = t;
				i++; j--;
			}
			cside = cside->next;
		}
	}

	return sides;
}
#line 3253 "tree.c"

struct rev_vtable__ const rev_vt__ = {
	&unary_vt__,
	rev_kind,
	"rev",
	(void (*)(expression *this__, int depth))rev_printtree__,
	(struct roll_value * (*)(expression *this__))rev_roll__,
	(void (*)(expression *this__, struct symtab * st))unary_set_symtab__,
	(void (*)(expression *this__, ordering_type ordering))unary_set_ordering__,
	(struct val_list * (*)(expression *this__))rev_eval__,
	(expression * (*)(expression *this__))unary_optimize__,
};

expression *elist_create(void)
{
	elist *node__ = (elist *)yynodealloc(sizeof(struct elist__));
	if(node__ == 0) return 0;
	node__->vtable__ = &elist_vt__;
	node__->kind__ = elist_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	return (expression *)node__;
}

expression *number_create(int num)
{
	number *node__ = (number *)yynodealloc(sizeof(struct number__));
	if(node__ == 0) return 0;
	node__->vtable__ = &number_vt__;
	node__->kind__ = number_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->num = num;
	return (expression *)node__;
}

expression *ifthenelse_create(expression * if_expr, expression * then_expr, expression * else_expr)
{
	ifthenelse *node__ = (ifthenelse *)yynodealloc(sizeof(struct ifthenelse__));
	if(node__ == 0) return 0;
	node__->vtable__ = &ifthenelse_vt__;
	node__->kind__ = ifthenelse_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->if_expr = if_expr;
	node__->then_expr = then_expr;
	node__->else_expr = else_expr;
	return (expression *)node__;
}

expression *variable_create(char * varname)
{
	variable *node__ = (variable *)yynodealloc(sizeof(struct variable__));
	if(node__ == 0) return 0;
	node__->vtable__ = &variable_vt__;
	node__->kind__ = variable_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->varname = varname;
	return (expression *)node__;
}

expression *sumrepdice_create(struct val_list * num_dice, int num_sides)
{
	sumrepdice *node__ = (sumrepdice *)yynodealloc(sizeof(struct sumrepdice__));
	if(node__ == 0) return 0;
	node__->vtable__ = &sumrepdice_vt__;
	node__->kind__ = sumrepdice_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->num_dice = num_dice;
	node__->num_sides = num_sides;
	return (expression *)node__;
}

expression *sumrepany_create(struct val_list * number, struct val_list * data)
{
	sumrepany *node__ = (sumrepany *)yynodealloc(sizeof(struct sumrepany__));
	if(node__ == 0) return 0;
	node__->vtable__ = &sumrepany_vt__;
	node__->kind__ = sumrepany_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->number = number;
	node__->data = data;
	return (expression *)node__;
}

expression *scat_create(expression * expr1, expression * expr2)
{
	scat *node__ = (scat *)yynodealloc(sizeof(struct scat__));
	if(node__ == 0) return 0;
	node__->vtable__ = &scat_vt__;
	node__->kind__ = scat_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	return (expression *)node__;
}

expression *rep_create(expression * expr1, expression * expr2)
{
	rep *node__ = (rep *)yynodealloc(sizeof(struct rep__));
	if(node__ == 0) return 0;
	node__->vtable__ = &rep_vt__;
	node__->kind__ = rep_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	return (expression *)node__;
}

expression *range_create(expression * expr1, expression * expr2)
{
	range *node__ = (range *)yynodealloc(sizeof(struct range__));
	if(node__ == 0) return 0;
	node__->vtable__ = &range_vt__;
	node__->kind__ = range_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	return (expression *)node__;
}

expression *lcat_create(expression * expr1, expression * expr2)
{
	lcat *node__ = (lcat *)yynodealloc(sizeof(struct lcat__));
	if(node__ == 0) return 0;
	node__->vtable__ = &lcat_vt__;
	node__->kind__ = lcat_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	return (expression *)node__;
}

expression *let_create(expression * expr1, expression * expr2, char * varname)
{
	let *node__ = (let *)yynodealloc(sizeof(struct let__));
	if(node__ == 0) return 0;
	node__->vtable__ = &let_vt__;
	node__->kind__ = let_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	node__->varname = varname;
	return (expression *)node__;
}

expression *foreach_create(expression * expr1, expression * expr2, char * varname)
{
	foreach *node__ = (foreach *)yynodealloc(sizeof(struct foreach__));
	if(node__ == 0) return 0;
	node__->vtable__ = &foreach_vt__;
	node__->kind__ = foreach_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	node__->varname = varname;
	return (expression *)node__;
}

expression *whiledo_create(expression * expr1, expression * expr2, char * varname)
{
	whiledo *node__ = (whiledo *)yynodealloc(sizeof(struct whiledo__));
	if(node__ == 0) return 0;
	node__->vtable__ = &whiledo_vt__;
	node__->kind__ = whiledo_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	node__->varname = varname;
	return (expression *)node__;
}

expression *plus_create(expression * expr1, expression * expr2)
{
	plus *node__ = (plus *)yynodealloc(sizeof(struct plus__));
	if(node__ == 0) return 0;
	node__->vtable__ = &plus_vt__;
	node__->kind__ = plus_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	return (expression *)node__;
}

expression *minus_create(expression * expr1, expression * expr2)
{
	minus *node__ = (minus *)yynodealloc(sizeof(struct minus__));
	if(node__ == 0) return 0;
	node__->vtable__ = &minus_vt__;
	node__->kind__ = minus_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	return (expression *)node__;
}

expression *multi_create(expression * expr1, expression * expr2)
{
	multi *node__ = (multi *)yynodealloc(sizeof(struct multi__));
	if(node__ == 0) return 0;
	node__->vtable__ = &multi_vt__;
	node__->kind__ = multi_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	return (expression *)node__;
}

expression *divi_create(expression * expr1, expression * expr2)
{
	divi *node__ = (divi *)yynodealloc(sizeof(struct divi__));
	if(node__ == 0) return 0;
	node__->vtable__ = &divi_vt__;
	node__->kind__ = divi_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	return (expression *)node__;
}

expression *mod_create(expression * expr1, expression * expr2)
{
	mod *node__ = (mod *)yynodealloc(sizeof(struct mod__));
	if(node__ == 0) return 0;
	node__->vtable__ = &mod_vt__;
	node__->kind__ = mod_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	return (expression *)node__;
}

expression *expo_create(expression * expr1, expression * expr2)
{
	expo *node__ = (expo *)yynodealloc(sizeof(struct expo__));
	if(node__ == 0) return 0;
	node__->vtable__ = &expo_vt__;
	node__->kind__ = expo_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	return (expression *)node__;
}

expression *first_create(expression * expr1, expression * expr2, filter_type type)
{
	first *node__ = (first *)yynodealloc(sizeof(struct first__));
	if(node__ == 0) return 0;
	node__->vtable__ = &first_vt__;
	node__->kind__ = first_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	node__->type = type;
	return (expression *)node__;
}

expression *last_create(expression * expr1, expression * expr2, filter_type type)
{
	last *node__ = (last *)yynodealloc(sizeof(struct last__));
	if(node__ == 0) return 0;
	node__->vtable__ = &last_vt__;
	node__->kind__ = last_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	node__->type = type;
	return (expression *)node__;
}

expression *high_create(expression * expr1, expression * expr2, filter_type type)
{
	high *node__ = (high *)yynodealloc(sizeof(struct high__));
	if(node__ == 0) return 0;
	node__->vtable__ = &high_vt__;
	node__->kind__ = high_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	node__->type = type;
	return (expression *)node__;
}

expression *low_create(expression * expr1, expression * expr2, filter_type type)
{
	low *node__ = (low *)yynodealloc(sizeof(struct low__));
	if(node__ == 0) return 0;
	node__->vtable__ = &low_vt__;
	node__->kind__ = low_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	node__->type = type;
	return (expression *)node__;
}

expression *comparison_create(expression * expr1, expression * expr2, filter_type type, comparison_type comp)
{
	comparison *node__ = (comparison *)yynodealloc(sizeof(struct comparison__));
	if(node__ == 0) return 0;
	node__->vtable__ = &comparison_vt__;
	node__->kind__ = comparison_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr1 = expr1;
	node__->expr2 = expr2;
	node__->type = type;
	node__->comp = comp;
	return (expression *)node__;
}

expression *negate_create(expression * expr)
{
	negate *node__ = (negate *)yynodealloc(sizeof(struct negate__));
	if(node__ == 0) return 0;
	node__->vtable__ = &negate_vt__;
	node__->kind__ = negate_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr = expr;
	return (expression *)node__;
}

expression *dice_create(expression * expr)
{
	dice *node__ = (dice *)yynodealloc(sizeof(struct dice__));
	if(node__ == 0) return 0;
	node__->vtable__ = &dice_vt__;
	node__->kind__ = dice_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr = expr;
	return (expression *)node__;
}

expression *sum_create(expression * expr)
{
	sum *node__ = (sum *)yynodealloc(sizeof(struct sum__));
	if(node__ == 0) return 0;
	node__->vtable__ = &sum_vt__;
	node__->kind__ = sum_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr = expr;
	return (expression *)node__;
}

expression *prod_create(expression * expr)
{
	prod *node__ = (prod *)yynodealloc(sizeof(struct prod__));
	if(node__ == 0) return 0;
	node__->vtable__ = &prod_vt__;
	node__->kind__ = prod_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr = expr;
	return (expression *)node__;
}

expression *count_create(expression * expr)
{
	count *node__ = (count *)yynodealloc(sizeof(struct count__));
	if(node__ == 0) return 0;
	node__->vtable__ = &count_vt__;
	node__->kind__ = count_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr = expr;
	return (expression *)node__;
}

expression *perm_create(expression * expr)
{
	perm *node__ = (perm *)yynodealloc(sizeof(struct perm__));
	if(node__ == 0) return 0;
	node__->vtable__ = &perm_vt__;
	node__->kind__ = perm_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr = expr;
	return (expression *)node__;
}

expression *sort_create(expression * expr)
{
	sort *node__ = (sort *)yynodealloc(sizeof(struct sort__));
	if(node__ == 0) return 0;
	node__->vtable__ = &sort_vt__;
	node__->kind__ = sort_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr = expr;
	return (expression *)node__;
}

expression *rev_create(expression * expr)
{
	rev *node__ = (rev *)yynodealloc(sizeof(struct rev__));
	if(node__ == 0) return 0;
	node__->vtable__ = &rev_vt__;
	node__->kind__ = rev_kind;
	node__->filename__ = yycurrfilename();
	node__->linenum__ = yycurrlinenum();
	node__->symtab = NULL;
	node__->ordering = caring;
	node__->expr = expr;
	return (expression *)node__;
}

struct yy_vtable__ {
	const struct yy_vtable__ *parent__;
	int kind__;
};

int yyisa__(const void *vtable__, int kind__)
{
	const struct yy_vtable__ *vt;
	vt = (const struct yy_vtable__ *)vtable__;
	while(vt != 0) {
		if(vt->kind__ == kind__)
			return 1;
		vt = vt->parent__;
	}
	return 0;
}

