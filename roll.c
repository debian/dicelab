#include <malloc.h>
#include <string.h>

#include "roll.h"

struct roll_value *new_roll_single(int num) {
	struct roll_value *ret = (struct roll_value*)malloc(
		sizeof(struct roll_value));
	ret->count = 1;
	ret->values = (int*)malloc(sizeof(int));
	ret->values[0] = num;
	return ret;
}

struct roll_value *new_roll_multi(int count) {
	struct roll_value *ret = (struct roll_value*)malloc(
		sizeof(struct roll_value));
	ret->count = count;
	ret->values = (int*)malloc(sizeof(int)*count);
	memset(ret->values, 0, sizeof(int)*count);
	return ret;
}

void free_roll(struct roll_value *v) {
	free(v->values);
	free(v);
}
